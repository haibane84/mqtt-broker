package co.bitfinder.awair.mqtt

import akka.actor.ActorSystem
import akka.dispatch.{PriorityGenerator, UnboundedStablePriorityMailbox}
import co.bitfinder.awair.mqtt.AwairTcpConnectionActor.Closing
import com.typesafe.config.Config

class ConnectionPriorityMailbox(
    settings: ActorSystem.Settings,
    config: Config)
  extends UnboundedStablePriorityMailbox(PriorityGenerator {
    case Closing => 0
    case _ => 1
  })

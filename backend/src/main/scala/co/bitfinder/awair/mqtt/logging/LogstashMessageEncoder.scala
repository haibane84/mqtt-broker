package co.bitfinder.awair.mqtt.logging

import java.nio.charset.Charset

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.ILoggingEvent
import com.github.danielwegener.logback.kafka.encoding.KafkaMessageEncoderBase
import org.joda.time.DateTime
import play.api.libs.json._

/**
  * Created by jay on 12/2/16.
  */
class LogstashMessageEncoder extends KafkaMessageEncoderBase[ILoggingEvent] {

  implicit val dateTimeWrites: Writes[DateTime] = Writes(dt => JsString(dt.toString))

  override def doEncode(event: ILoggingEvent): Array[Byte] = {
    val message = event.getLevel match {
      case Level.ERROR => event.getMessage + "\n" + event.getThrowableProxy.getClassName + ": " + event.getThrowableProxy.getMessage + "\n\t" + event.getThrowableProxy.getStackTraceElementProxyArray.map(_.getSTEAsString).mkString("\n\t")
      case _ => event.getMessage
    }
    val base = Json.obj(
      "@version" -> "1",
      "@timestamp" -> new DateTime(event.getTimeStamp),
      "level" -> event.getLevel.toString,
      "type" -> "awair-messaging-service-log",
      "tags" -> List("awair-mqtt-broker"),
      "message" -> message,
      "thread" -> event.getThreadName)
    val args = event.getArgumentArray.foldLeft(Json.obj()) {
      case (acc, (k: String, v: Int)) => acc + (k -> JsNumber(v))
      case (acc, (k: String, v: Long)) => acc + (k -> JsNumber(v))
      case (acc, (k: String, v: Double)) => acc + (k -> JsNumber(v))
      case (acc, (k: String, v: String)) => acc + (k -> JsString(v))
      case (acc, (k: String, v: Boolean)) => acc + (k -> JsBoolean(v))
      case (acc, (k: String, v)) => acc + (k -> JsString(v.toString))
      case (acc, _) => acc
    }
    val json = base ++ args
    json.toString.getBytes(Charset.forName("UTF-8"))
  }
}
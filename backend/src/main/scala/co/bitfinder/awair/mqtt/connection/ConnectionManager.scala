package co.bitfinder.awair.mqtt.connection

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import co.bitfinder.awair.mqtt.AwairTcpConnectionActor
import kamon.Kamon
import kamon.metric.instrument.Counter
import play.api.libs.ws.WSClient

/**
  * Created by gojaedo on 2017. 3. 6..
  */

object ConnectionManager {
  def props(host:String, port:Int, sessionManagerSupervisor: ActorRef, kafkaHandler: ActorRef, wsClient: WSClient, metricsActor: Option[ActorRef]) =
    Props(new ConnectionManager(host, port, sessionManagerSupervisor, kafkaHandler, wsClient, metricsActor))
}

class ConnectionManager(host: String, port: Int, sessionManagerSupervisor: ActorRef, kafkaHandler: ActorRef, wsClient: WSClient, metricsActor: Option[ActorRef]) extends Actor with ActorLogging {
  val connectionCounter: Counter = Kamon.metrics.counter("connection-counter")

  override def preStart(): Unit = {
    IO(Tcp)(context.system) ! Bind(self, new InetSocketAddress(host, port))
  }

  override def postRestart(thr: Throwable): Unit = context stop self

  def receive: Receive = {
    case Bound(localAddress) =>
      log.info("listening on port {}", localAddress.getPort)

    case CommandFailed(Bind(_, address, _, _, _)) =>
      log.warning(s"cannot bind to [$address]")
      context stop self

    case Connected(remote, _) =>
      val tcpConnActorRef = sender()
      val clientConnActorName = remote.getHostString + ":" + remote.getPort
      val clientConnActorRef = context.actorOf(AwairTcpConnectionActor.props(sessionManagerSupervisor, kafkaHandler, wsClient, tcpConnActorRef, metricsActor).withDispatcher("mqtt-dispatcher"), clientConnActorName)
      log.info(s"Accepted new connection from $remote and trying to create new AwairTcpConnectionActor(${clientConnActorRef.path})")

      this.connectionCounter.increment()
      tcpConnActorRef ! Register(clientConnActorRef, keepOpenOnPeerClosed = false)
  }
}

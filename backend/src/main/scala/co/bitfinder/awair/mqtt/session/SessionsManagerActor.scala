package co.bitfinder.awair.mqtt.session

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Put}
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor.{ConnectWithEntity, DisconnectWithEntity, PublishWithEntity}
import co.bitfinder.awair.mqtt.session.SessionActor.ResetSession
import co.bitfinder.awair.mqtt.session.SessionsManagerActor._
import co.bitfinder.awair.{Device, Entity, Org, User}

import scala.concurrent.duration._
import kamon.Kamon

import scala.util.Try

object SessionsManagerActor {
  case class SessionInfo(entity: Entity, connection: Boolean)
  case class PublishInfo(entity: Entity, connection: Boolean, payload: String)

  case class GetSessions()
  case class GetSession(sessionName: String)
  case class RecordSession()
  case object NotFoundSession

  def props(bus: ActorRef, sessionsSupervisor: ActorRef, metricsActor: Option[ActorRef]) =
    Props(new SessionsManagerActor(bus, sessionsSupervisor: ActorRef, metricsActor))
}

class SessionsManagerActor(bus: ActorRef, sessionsSupervisor: ActorRef, metricsActor: Option[ActorRef]) extends Actor with ActorLogging {
  import context.dispatcher

  private val sessionHistogram = Kamon.metrics.histogram("session-histogram")
  private val mediator = DistributedPubSub(context.system).mediator
  mediator ! Put(self)

  context.system.scheduler.schedule(10 seconds, 10 seconds, self, RecordSession())

  def getOrCreate(actor_name: String, clean_session: Boolean): ActorRef = {
    val c = context.child(actor_name)
    c match {
      case Some(a) if !clean_session =>
        log.debug(s"Get new Session {} without clean_session", actor_name)
        a
      case Some(a) =>
        log.debug(s"Get new Session {} with clean_session so set reset", actor_name)
        a ! ResetSession
        a
      case None =>
        log.debug(s"Create new Session {}", actor_name)
        val a = context.actorOf(Props(new SessionActor(bus, sessionsSupervisor, metricsActor)), name = actor_name)
        context.watch(a)
        a
    }
  }

  def receive: Receive = {
    case ConnectWithEntity(connect, entity) =>
      val name = entity.buildActorNameBy(connect.clientId)
      val session = getOrCreate(name, connect.connect_flags.clean_session)
      mediator ! Publish(entity.toMediatorTopic, SessionInfo(entity, connection = true))
      sender ! session

    case PublishWithEntity(publish, entity, _) if entity.isInstanceOf[Device] =>
      mediator ! Publish(entity.toMediatorTopic, PublishInfo(entity, connection = true, publish.payload))

    case DisconnectWithEntity(_, entity, _) =>
      mediator ! Publish(entity.toMediatorTopic, SessionInfo(entity, connection = false))

    case g: GetSession if g.sessionName.matches("^device-\\w+-\\d+") =>
      val session = getOrCreate(g.sessionName, false)
      sender ! session

    case g: GetSession =>
      context.child(g.sessionName) match {
        case Some(a) =>
          sender() ! a
        case None =>
          sender() ! NotFoundSession
      }

    case Terminated(ref) =>
      val entityO = Try {
        ref.path.name match {
          case t if t.matches("^org-.+") => Org
          case t if t.matches("^user-.+") => User("^user-(\\d+)-.*".r.findAllIn(t).toList.head.toInt)
          case t if t.matches("^device-.+") =>
            val tokens = "^device-(\\w+)-(\\d+)".r.findAllIn(t).toList
            Device(tokens.head, tokens(1).toInt)
        }
      }.toOption
      entityO match {
        case Some(_: Device) => log.warning("DeviceSession should not be destroyed")
        case Some(e) => mediator ! Publish(e.toMediatorTopic, SessionInfo(e, connection = false))
        case None =>
      }
      log.debug(s"${ref.path.name} session is destroyed")

    case _: GetSessions =>
      sender() ! context.children.toList.map(_.path.toSerializationFormat)

    case _: RecordSession =>
      sessionHistogram.record(context.children.size)

    case x => log.error("Unexpected " + x)
  }
}

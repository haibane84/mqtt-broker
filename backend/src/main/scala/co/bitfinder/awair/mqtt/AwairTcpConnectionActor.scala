package co.bitfinder.awair.mqtt

import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import akka.io.Tcp.{PeerClosed, Connect => _, _}
import akka.pattern.ask
import akka.util.{ByteString, Timeout}
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor
import co.bitfinder.awair.mqtt.logging.MqttControlPacketLogger
import co.bitfinder.awair.{Entity, auth}
import co.bitfinder.mqtt.Helpers._
import co.bitfinder.mqtt.{Disconnect, _}
import com.typesafe.config.ConfigFactory
import org.apache.commons.lang3.exception.ExceptionUtils
import play.api.libs.ws.WSClient
import scodec.Attempt.Failure

import scala.concurrent.duration._

object AwairTcpConnectionActor {
  private val jwtSecret = ConfigFactory.load().getString("jwt.secret")

  case class ReceivedPacket(packet: Packet, entity: Option[Entity] = None, clientId: Option[String] = None)
  case class SendingPacket(packet: Packet)
  case object Closing
  case object Ack extends Event

  val maxStored: Long = 100000000L
  val highWatermark: Long = maxStored * 5 / 10
  val lowWatermark: Long = maxStored * 3 / 10

  def props(sessionManger: ActorRef, kafkaSender: ActorRef, wsClient: WSClient, connection: ActorRef, metricsActor: Option[ActorRef]) =
    Props(new AwairTcpConnectionActor(sessionManger, kafkaSender, wsClient, connection, metricsActor))
}

class AwairTcpConnectionActor(sessionManager: ActorRef,
                              kafkaSender: ActorRef,
                              wsClient: WSClient,
                              connection: ActorRef,
                              metricsActor: Option[ActorRef]) extends Actor with ActorLogging {

  import AwairTcpConnectionActor._
  import context.dispatcher

  private implicit val client = wsClient
  private implicit val timeout = Timeout(3.seconds)
  private val mqtt = context.actorOf(Props(new MqttConnectionActor(sessionManager, metricsActor)))

  var authToken: String = _
  var entityO: Option[Entity] = None
  var clientId: String = _
  var storage = Vector.empty[ByteString]
  var stored = 0L
  var closing = false
  var suspended = false

  context watch connection

  override def postStop(): Unit = {
    super.postStop()
    entityO.foreach(MqttControlPacketLogger.logDisconnection)
  }

  private def closeMqttConn(block: => Unit) = (mqtt ? PeerClosed) recover {
    case ex =>
      val stackTrace = ExceptionUtils.getStackTrace(ex)
      log.warning(s"[${self.path.name}] got error while trying to send PeerClose to MQTT connection actor: $stackTrace")
  } onComplete(_ => block)

  val waitingForAck: Receive = {
    case Received(data) =>
      handleReceivedData(data)

    case SendingPacket(p) =>
      val bits = PacketsHelper.encode(p)
      val envelope = ByteString(bits.require.toByteArray)
      log.debug("[{}] Buffering data {}", self.path.name, p)
      buffer(envelope)

    case Ack =>
      log.debug("[{}] got ack", self.path.name)
      acknowledge()

    case PeerClosed =>
      closing = true

    case Closing =>
      connection ! Close
      context stop self

    case x =>
      log.error("[{}] Unexpected Message {}", self.path.name, x.getClass.getCanonicalName)
      connection ! Close
      context stop self
  }

  def receive: Receive = {
    case Received(data) =>
      handleReceivedData(data)

    case SendingPacket(p) =>
      val bits = PacketsHelper.encode(p)
      val envelope = ByteString(bits.require.toByteArray)
      log.debug("[{}] SendingPacket data {}", self.path.name, p)
      buffer(envelope)
      connection ! Write(envelope, Ack)

      context.become(waitingForAck, discardOld = false)

    case Closing =>
      connection ! Close
      context stop self

    case PeerClosed =>
      connection ! Close
      context stop self

    case ErrorClosed(cause: String) =>
      log.warning(cause)
      context stop self

    // for health check connection termination
    case _: Terminated =>

    case x =>
      log.error("[{}] Unexpected Message {}", self.path.name, x.getClass.getCanonicalName)
      connection ! Close
      context stop self
  }

  private def authorizeAndSendToMqtt(p: Packet) = {
    entityO.foreach(e => MqttControlPacketLogger.logInbound(e, clientId, p))
    auth.authorise(entityO, p) map {
      case true =>
        log.debug("[{}] Handle Packet for Pre Processor => {}", self.path.name, p)
        p match {
          case con: Connect => clientId = con.clientId
          case pub: Publish =>
            log.debug("Send {} to Kafka", pub)
            kafkaSender ! pub
          case _ =>
        }
        mqtt ! ReceivedPacket(p, entityO, Some(clientId))
      case false =>
        log.info("[{}] entity: {}, packet {} unauthorized", self.path.name, entityO, p)
        mqtt ! ReceivedPacket(Disconnect(p.header))
        context.stop(self)
    } recover {
      case e =>
        log.error(e, "[{}] Got Error while try to authorize so try to disconnect client", self.path.name)
        mqtt ! ReceivedPacket(Disconnect(p.header))
        context.stop(self)
    }
  }

  private def handleReceivedData(data: ByteString) = {
    val packets = PacketsHelper.decode(data.toBitVector)
    log.debug("[{}] Received data {}", self.path.name, packets)
    packets foreach {
      case Left(originalPacket: Packet) =>
        val packet = sanitizePacket(originalPacket)
        packet match {
          case c: Connect =>
            authToken = c.user.getOrElse("")
            entityO = Entity.fromJwt(authToken, jwtSecret)
            clientId = c.clientId
            log.debug("[{}] Initialize Connection and Got Entity {} for clientId: {}", self.path.name, entityO, clientId)
            authorizeAndSendToMqtt(c)
          case _ =>
            authorizeAndSendToMqtt(packet)
        }
      case Right(f: Failure) =>
        log.warning("[{}] Decoding failed with {}", self.path.name, f)
        connection ! Close
        closeMqttConn {
          // the assumption here is that if context stop gets called multiple times, it results in NPE (which we do see in production)
          if (context != null) context stop self
        }
    }
  }

  private def buffer(data: ByteString): Unit = {
    storage :+= data
    stored += data.size

    if (stored > maxStored) {
      log.warning(s"[${self.path.name}] drop connection (buffer overrun)")
      context stop self
    } else if (stored > highWatermark) {
      log.warning(s"[${self.path.name}] suspending reading")
      connection ! SuspendReading
      suspended = true
    }
  }

  private def acknowledge(): Unit = {
    require(storage.nonEmpty, "storage was empty")

    val size = storage(0).size
    stored -= size
    storage = storage drop 1

    if (suspended && stored < lowWatermark) {
      log.debug("resuming reading")
      connection ! ResumeReading
      suspended = false
    }

    if (storage.isEmpty) {
      if (closing) context stop self
      else context.unbecome()
    } else connection ! Write(storage(0), Ack)
  }

  private def sanitizePacket(packet: Packet) = packet match {
    case conn: Connect =>
      val newClientId = conn.clientId match {
        case "android-client" => "android-client-" + UUID.randomUUID.toString
        case "" => UUID.randomUUID().toString //make sure no empty clientId
        case _ => conn.clientId
      }
      val isRandomClientId = if (newClientId.contains("IOS") || newClientId.contains("device-awairglow"))
        conn.connect_flags.clean_session
      else
        true // for Android and Awair v1
      conn.copy(clientId = newClientId, connect_flags = conn.connect_flags.copy(clean_session = isRandomClientId))

    case sub: Subscribe =>
      sub.copy(topics = sub.topics.map(x => (x._1, 0)))

    case pub: Publish =>
      pub.copy(header = pub.header.copy(qos = 0))

    case _ => packet
  }

}

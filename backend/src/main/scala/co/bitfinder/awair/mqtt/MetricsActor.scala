package co.bitfinder.awair.mqtt

import akka.actor.{Actor, ActorLogging}
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor.TrackablePublish
import co.bitfinder.mqtt.{Publish, Subscribe}
import kamon.Kamon
import kamon.metric.instrument.{Counter, Histogram}
import org.joda.time.{DateTime, Duration}

import scala.collection.mutable
import scala.concurrent.duration._

/**
  * Created by jeado on 2017. 3. 28..
  */
object MetricsActor {
  case class PubToSubscriberDate(id:String, msg: Subscribe, date: DateTime)
  case object CheckRemain
}
class MetricsActor extends Actor with ActorLogging {
  import co.bitfinder.awair.mqtt.MetricsActor._
  import context.dispatcher

  val successCounter: Counter = Kamon.metrics.counter("pubsub-success")
  val skipPubCounter: Counter = Kamon.metrics.counter("skipped-pub")
  val latencyHistogram: Histogram = Kamon.metrics.histogram("latency-histogram")

  var map = mutable.Map.empty[String, TrackablePublish]
  context.system.scheduler.schedule(10 seconds, 10 seconds, self, CheckRemain)

  override def receive: Receive = {
    case tp: TrackablePublish =>
      map.get(tp.id) match {
        case Some(pub) =>
          if(tp.msg.payload != pub.msg.payload) {
            log.warning(s"It seems id is corrupted pub $tp pub for sub $pub")
            map.remove(tp.id)
          } else {
            val duration = new Duration(pub.date, tp.date)
            if(duration.getMillis > 0) latencyHistogram.record(duration.getMillis)
            successCounter.increment()
            log.debug(s"Found pub($pub) so sub and ${duration.getMillis}")
            map.remove(tp.id)
          }
        case None =>
          log.debug(s"Not so first so keep it")
          map.put(tp.id, tp)
      }

    case CheckRemain =>
      map.foreach(s => {
        val duration = new Duration(s._2.date, new DateTime())
        if(duration.getStandardSeconds > 10) map.remove(s._1)
      })
      skipPubCounter.increment(map.size)
      log.info(s"remain msg => ${map.size} in 10 secs")

    case _ =>
      log.warning("got unexpected msg")
  }
}

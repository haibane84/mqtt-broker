package co.bitfinder.awair.mqtt

import java.util.Properties

import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.cluster.Cluster
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings, ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import co.bitfinder.awair.Entity
import co.bitfinder.awair.kafka.KafkaProducerService
import co.bitfinder.awair.mqtt.connection.{ConnectionManager, EventBusActor}
import co.bitfinder.awair.mqtt.session.{SessionManagerSupervisor, SessionsManagerActor}
import co.bitfinder.mqtt._
import com.typesafe.config.{Config, ConfigFactory}
import kamon.Kamon
import org.slf4j.LoggerFactory
import play.api.libs.ws.ahc.{AhcConfigBuilder, AhcWSClient, AhcWSClientConfig}

/**
  * Created by jeado on 2016. 12. 30..
  */
object Main extends App {
  val logger = LoggerFactory.getLogger(Main.getClass)
  
  val nodeConfig = NodeConfig parse args
  val jwtSecret = ConfigFactory.load().getString("jwt.secret")

  nodeConfig match {
    case Some(c) =>
      Kamon.start()
  
      implicit val system = ActorSystem(c.clusterName, c.config)
      implicit val materializer = ActorMaterializer()
  
      system.actorOf(ClusterSingletonManager.props(
        singletonProps = Props[EventBusActor],
        terminationMessage = PoisonPill,
        settings = ClusterSingletonManagerSettings(system).withRole("backend")),
        name = "event-bus")
  
      val bus = system.actorOf(ClusterSingletonProxy.props(
        singletonManagerPath = "/user/event-bus",
        ClusterSingletonProxySettings(system).withRole("backend")),
        name = "event-bus-proxy")

      val metricsActor = if (c.config.getBoolean("metrics.enable")) {
        system.actorOf(ClusterSingletonManager.props(
          singletonProps = Props[MetricsActor],
          terminationMessage = PoisonPill,
          settings = ClusterSingletonManagerSettings(system).withRole("backend")),
          name = "metrics-actor")

        Some(
          system.actorOf(ClusterSingletonProxy.props(
            singletonManagerPath = "/user/metrics-actor",
            ClusterSingletonProxySettings(system).withRole("backend")),
            name = "metrics-actor-proxy"))
      } else None

      val sessionManagerSupervisor = system.actorOf(SessionManagerSupervisor.props.withDispatcher("mqtt-thread-pool-dispatcher"), "sessionManagerSupervisor")
      system.actorOf(SessionsManagerActor.props(bus, sessionManagerSupervisor, metricsActor), "sessionManager")

      val host = c.config.getString("host")
      val port = c.config.getInt("port")
  
      val kafkaHandler = system.actorOf(KafkaProducerService.props(buildKafkaProperties(c.config)), "kafkaProducer")
  
      val ahcWsConfig = new AhcConfigBuilder(AhcWSClientConfig()).configure.build
      implicit val wsClient = new AhcWSClient(ahcWsConfig)
  
      case class MqttMessage(entityO: Option[Entity], clientId: String, packet: Packet, rawData: ByteString)
  
      Cluster(system) registerOnMemberUp {
        system.actorOf(ConnectionManager.props(host, port, sessionManagerSupervisor, kafkaHandler, wsClient, metricsActor), "connection-manager")
        logger.info(s"ActorSystem ${system.name} started successfully with $host:$port")
      }

      sys.addShutdownHook {
        logger.info(s"Shutdown ActorSystem ${system.name}")
        Kamon.shutdown()
        system.registerOnTermination(System.exit(0))
        system.terminate()
      }
    case None =>
      logger.error(s"Configuration is wrong. Shutdown the system")
      System.exit(0)
  }

  private def buildKafkaProperties(conf: Config) = {
    val props = new Properties
    props.put("bootstrap.servers", conf.getString("kafka.uris"))
    props.put("retries", "0")
    props.put("batch.size", "16384")
    props.put("linger.ms", "1")
    props.put("buffer.memory", "33554432")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props
  }
}
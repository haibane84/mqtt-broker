package co.bitfinder.awair.mqtt.session

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.routing.ConsistentHashingRouter.ConsistentHashableEnvelope
import akka.routing.{FromConfig, GetRoutees}
import co.bitfinder.awair.Device
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor.{ConnectWithEntity, DisconnectWithEntity, PublishWithEntity}
import co.bitfinder.awair.mqtt.session.SessionsManagerActor.GetSession

/**
  * Created by gojaedo on 2017. 2. 8..
  */

object SessionManagerSupervisor {
  def props: Props = Props(new SessionManagerSupervisor())
  case class GetDeviceSession(deviceEntity: Device)
}

class SessionManagerSupervisor extends Actor with ActorLogging {
  import co.bitfinder.awair.mqtt.session.SessionManagerSupervisor._
  val sessionManagerRouter: ActorRef = context.actorOf(FromConfig.props(Props.empty), "sessionManagerRouter")

  override def receive: Receive = {
    case c: ConnectWithEntity =>
      sessionManagerRouter forward ConsistentHashableEnvelope(c, c.entity.buildActorNameBy(c.connect.clientId))

    case p@ PublishWithEntity(_, entity, clientId) =>
      entity match {
        case _: Device => sessionManagerRouter forward ConsistentHashableEnvelope(p, entity.buildActorNameBy(clientId))
        case _ =>
      }

    case d: DisconnectWithEntity =>
      sessionManagerRouter forward ConsistentHashableEnvelope(d, d.entity.buildActorNameBy(d.clientId))

    case GetDeviceSession(entity) =>
      val key = entity.buildActorNameBy("DUMMY")
      sessionManagerRouter forward ConsistentHashableEnvelope(GetSession(key), key)

    case m@GetRoutees => sessionManagerRouter forward m

    case x => log.error("Unexpected " + x)
  }
}

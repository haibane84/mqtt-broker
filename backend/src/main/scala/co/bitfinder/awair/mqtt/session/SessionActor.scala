package co.bitfinder.awair.mqtt.session

import java.util.UUID

import akka.actor.{ActorRef, FSM, Props, Terminated}
import akka.pattern._
import akka.util.Timeout
import co.bitfinder.awair._
import co.bitfinder.awair.mqtt.connection.{PublishPayload, _}
import co.bitfinder.awair.mqtt.logging.MqttControlPacketLogger
import co.bitfinder.awair.mqtt.session.SessionActor._
import co.bitfinder.awair.mqtt.session.SessionManagerSupervisor.GetDeviceSession
import co.bitfinder.mqtt._
import kamon.Kamon
import kamon.metric.instrument.Counter
import org.joda.time.DateTime
import org.slf4j.LoggerFactory

import scala.concurrent.duration._

object SessionActor {
  sealed trait SessionState
  case object WaitingForNewSession extends SessionState
  case object SessionConnected extends SessionState

  sealed trait SessionBag {
    val messageId: Int
    val cleanSession: Boolean
    val subs: List[DirectSubscription]
    val sending: List[Either[Publish, Pubrel]]
  }
  case class SessionWaitingBag(
      sending: List[Either[Publish, Pubrel]],
      stashed: List[PublishPayload],
      subs: List[DirectSubscription],
      messageId: Int,
      cleanSession: Boolean) extends SessionBag

  case class SessionConnectedBag(
      connection: ActorRef,
      cleanSession: Boolean,
      messageId: Int,
      lastPacket: Long,
      sending: List[Either[Publish, Pubrel]],
      subs: List[DirectSubscription],
      entity: Entity,
      will: Option[Publish],
      clientId: String) extends SessionBag

  case object ResetSession
  case object GetSessionInfo
  case object ConnectionLost
  case object WrongState
  case class CheckKeepAlive(period: FiniteDuration)
  case object KeepAliveTimeout
  case object TrySending

  case class DirectSubscribe(topic: String, session: ActorRef, qos: Int)
  case class DirectSubscription(topic: String, session: ActorRef, qos: Int)

  def props(bus: ActorRef, sessionsSupervisor: ActorRef, metricsActor: Option[ActorRef]): Props =
    Props(new SessionActor(bus, sessionsSupervisor, metricsActor))
}

class SessionActor(bus: ActorRef, sessionsSupervisor: ActorRef, metricsActor: Option[ActorRef]) extends FSM[SessionState, SessionBag] {
  var sessionCounter: Option[Counter] = None
  import co.bitfinder.awair.mqtt.connection.MqttConnectionActor._
  import context.dispatcher

  implicit val timeout = Timeout(3.seconds)

  private def publishToSubscriptions(subs: List[DirectSubscription], publish: Publish, id: String = "") = subs
    .filter(_.topic == publish.topic)
    .foreach { t =>
      log.debug(s"publish $publish by subscription $t")
      t.session ! PublishPayload(publish, qos = math.min(t.qos, publish.header.qos), id = id)
    }

  private def sendToDevice(device: Device, payload: Any) = sessionsSupervisor ? GetDeviceSession(device) map {
    case session: ActorRef =>
      log.debug(s"[${context.self.path}] publish $payload to $session")
      session ! payload
    case _ =>
      log.warning(s"[${context.self.path}] could not find device session for $device")
  } recover {
    case e =>
      log.error(e, s"[${context.self.path}]Got Error while GetDeviceSession($device)")
  }

  startWith(WaitingForNewSession, SessionWaitingBag(List(), List(), List(), 1, cleanSession = true))

  when(WaitingForNewSession) {
    case Event(ConnectWithEntity(connect, entity), bag: SessionWaitingBag) =>
      if (connect.clientId.length == 0 && !connect.connect_flags.clean_session) {
        MqttControlPacketLogger.logOutbound(entity, connect.clientId, connect)
        sender ! Connack(Header(dup = false, 0, retain = false), 2)
        stay
      } else {
        MqttControlPacketLogger.logOutbound(entity, connect.clientId, connect)
        sender ! Connack(Header(dup = false, 0, retain = false), 0)
        if (!connect.connect_flags.clean_session) {
          log.debug("current stashed is " + bag.stashed)
          bag.stashed.foreach(x => {
            log.debug("publishing stashed " + x)
            self ! x
          })
          log.debug("current sending is " + bag.sending)
          bag.sending.foreach {
            case Left(p) =>
              val pp = p.copy(header = p.header.copy(dup = true))
              log.debug("publishing sending " + pp)
              sender ! pp
            case Right(p) =>
              log.debug("publishing sending " + p)
              sender ! p
          }
        }
        if (connect.connect_flags.keep_alive > 0) {
          val delay = connect.connect_flags.keep_alive.seconds
          log.info(s"[${context.self.path}] start session and keep alive $delay")
          context.system.scheduler.scheduleOnce(delay, self, CheckKeepAlive(delay))(context.system.dispatcher)
        } else {
          log.info(s"[${context.self.path}] start session without keepalive")
        }
        val will = if (connect.connect_flags.will_flag)
          Some(Publish(Header(dup = false, connect.connect_flags.will_qos, retain = connect.connect_flags.will_retain), connect.topic.get,0, connect.message.get))
        else None
        sessionCounter = Some(Kamon.metrics.counter("session-counter", Map("type" -> entity.buildActorNameBy(connect.clientId))))
        sessionCounter.foreach(_.increment)
        goto(SessionConnected) using SessionConnectedBag(sender, connect.connect_flags.clean_session, bag.messageId,System.currentTimeMillis, List(), bag.subs, entity, will, connect.clientId)
      }

    /*
     * This is used only by Org entity to relay its device event msg to User.
     */
    case Event(TrackablePublish(id, msg, _), b: SessionWaitingBag) =>
      Topic.fromMqttTopic(msg.topic) match {
        case Some(_: DeviceEvent) => publishToSubscriptions(b.subs, msg, id)
        case _ =>
      }
      stay

    case Event(p @ PublishPayload(pp: Publish, _, _, _), b: SessionWaitingBag) =>
      if (pp.header.qos > 0) {
        val sp = p.copy(payload = pp.copy(header = pp.header.copy(dup = true)))
        log.debug(s"[${context.self.path.name}] stashing " + sp)
        goto(WaitingForNewSession) using b.copy(stashed = b.stashed :+ sp)
      } else {
        log.debug(s"[${context.self.path.name}] skipping stash " + p)
        stay
      }

    case Event(ds @ DirectSubscribe(topic, session, qos), b: SessionWaitingBag) =>
      context watch session
      log.warning(s"[${context.self.path.name}] Got DirectSubscribe $ds even if this session`s MqttConnection is closed")
      if (!b.subs.contains(DirectSubscription(topic, session, qos))) {
        stay using b.copy(subs = b.subs :+ DirectSubscription(topic, session, qos))
      } else {
        stay
      }

    case Event(t: Terminated, b: SessionWaitingBag) =>
      log.debug(s"[${context.self.path.name}] remove subscription for terminated session ${t.actor}")
      stay() using b.copy(subs = b.subs.filterNot(sub => sub.session == t.actor))

    case Event(GetSessionInfo, b: SessionWaitingBag) =>
      sender() ! b
      stay()

    case Event(CheckKeepAlive(_), _: SessionWaitingBag) =>
      stay()

    case Event(p: Packet, _) =>
      log.warning(s"[${context.self.path.name}] Got wrong packet($p) in wrong state => $stateData")
      sender ! WrongState
      stay
  }

  when(SessionConnected) {
    case Event(CheckKeepAlive(keepAlive), b: SessionConnectedBag) =>
      log.debug("Last package was " + new java.util.Date(b.lastPacket))
      if ((System.currentTimeMillis - keepAlive.toMillis) < b.lastPacket) {
        log.debug(s"${context.self.path.name}] still within the KeepAlive period: $keepAlive")
        context.system.scheduler.scheduleOnce(keepAlive, self, CheckKeepAlive(keepAlive))(context.system.dispatcher)
        stay
      } else {
        log.info("exceeded KeepAlive period")
        b.connection ! KeepAliveTimeout
        self ! ConnectionLost
        stay
      }

    case Event(s: SubscribeWithEntity, b: SessionConnectedBag) =>
      log.info(s"[${context.self.path.name}] Handling subscribe message: ${s.subscribe}")
      s.subscribe.topics.foreach {
        case (topic: String, qos: Int) => s.entity match {
          case Org => bus ! BusSubscribe(topic, self, qos)
          case _ => Topic.fromMqttTopic(topic) match {
            case Some(x: DeviceEvent) =>
              sendToDevice(x.device, DirectSubscribe(topic, self, qos))
            case Some(_: DeviceCommand) => s.entity match {
              case _: Device =>
              case _ => log.warning(s"[${context.self.path.name}] Only Device can subscribe CommandEvent")
            }
            case None =>
              log.info(s"[${context.self.path.name}] Got invalid topic : $topic")
              bus ! BusSubscribe(topic, self, qos)
          }
        }
      }
      MqttControlPacketLogger.logOutbound(s.entity, b.clientId, s.subscribe)
      sender() ! Suback(Header(), s.subscribe.message_identifier, s.subscribe.topics.map(_._2))
      stay using b.copy(lastPacket = System.currentTimeMillis())

    case Event(ds @ DirectSubscribe(topic, session, qos), b: SessionConnectedBag) =>
      context watch session
      log.debug(s"[${context.self.path}] Got DirectSubscribe $ds")
      if (!b.subs.contains(DirectSubscription(topic, session, qos))) {
        stay using b.copy(lastPacket = System.currentTimeMillis(), subs = b.subs :+ DirectSubscription(topic, session, qos))
      } else {
        stay using b.copy(lastPacket = System.currentTimeMillis())
      }

    case Event(t: Terminated, b: SessionConnectedBag) =>
      log.debug(s"[${context.self.path}] remove subscription for terminated session ${t.actor}")
      stay() using b.copy(subs = b.subs.filterNot(sub => sub.session == t.actor))

    case Event(p: Unsubscribe, b: SessionConnectedBag) =>
      p.topics.foreach(bus ! BusUnsubscribe(_, self))
      sender ! Unsuback(Header(dup = false, 0, retain = false), p.message_identifier)
      stay using b.copy(lastPacket = System.currentTimeMillis)

    case Event(tp @ TrackablePublish(id, msg, _), b: SessionConnectedBag) =>
      // Will never reach these because QoS will always be 0 from the beginning.
      if (msg.header.qos == 1) sender ! Puback(Header(dup = false, 0, retain = false), msg.message_identifier)
      if (msg.header.qos == 2) sender ! Pubrec(Header(dup = false, 0, retain = false), msg.message_identifier)

      // Used to clear retain when sent with empty payload. Not used now.
      if (msg.payload.length == 0 && !msg.header.retain) {
        log.warning(s"[${context.self.path.name}] got message with retain false and empty payload")
        stay using b.copy(lastPacket = System.currentTimeMillis)
      } else {
        Topic.fromMqttTopic(msg.topic) match {
          case Some(c: DeviceCommand) =>
            sendToDevice(c.device, PublishPayload(msg, qos = msg.header.qos, id = id))
          case Some(e: DeviceEvent) =>
            log.info(s"[${context.self.path.name}] sending to ${b.subs}")
            b.entity match {
              case Org => sendToDevice(e.device, tp)
              case _ => publishToSubscriptions(b.subs, msg, id)
            }
          case None =>
        }
        bus ! BusPublish(msg.topic, msg, msg.header.retain, msg.header.retain && msg.payload.length == 0, tp.id)
        stay using b.copy(lastPacket = System.currentTimeMillis())
      }

    case Event(ConnectionLost, b @ SessionConnectedBag(_, _, _, _, _, _, entity, will, _)) =>
      log.debug(s"[${context.self.path}] got ConnectionLost")
      if (will.isDefined) {
        val p = b.will.get.copy(message_identifier = b.messageId)
        log.info(s"[${context.self.path}] ConnectionLost and Will is defined, so publishing Will Message $p to bus and local subscriptions ${b.subs}")
        publishToSubscriptions(b.subs, p, UUID.randomUUID().toString)
        bus ! BusPublish(p.topic, p, p.header.retain, p.header.retain && p.payload.length == 0)
      }
      if (b.cleanSession && !b.entity.isInstanceOf[Device]) {
        bus ! BusDetach(self)
        stop(FSM.Shutdown)
      } else {
        log.debug(s"[${context.self.path}] Stay in idle")
        goto(WaitingForNewSession) using SessionWaitingBag(b.sending, List(), b.subs, 1, b.cleanSession)
      }

    case Event(p: Pubrec, b: SessionConnectedBag) =>
      val pubrel = Pubrel(Header(dup = false, 1, retain = false), p.message_identifier)
      b.connection ! pubrel
      stay using b.copy(lastPacket = System.currentTimeMillis(), sending = b.sending.filter {
        case Left(publish) if publish.message_identifier == p.message_identifier => true
        case _ => false
      } :+ Right(pubrel))

    case Event(p: Pubrel, b: SessionConnectedBag) =>
      sender ! Pubcomp(Header(dup = false, 0, retain = false), p.message_identifier)
      stay using b.copy(lastPacket = System.currentTimeMillis())

    case Event(p: Puback, b: SessionConnectedBag) =>
      log.debug(s"received $p")
      stay using b.copy(lastPacket = System.currentTimeMillis(), sending = b.sending.filter {
        case Left(publish) if publish.message_identifier == p.message_identifier => true
        case _ => false
      })

    case Event(p: Pubcomp, b:SessionConnectedBag) =>
      log.debug(s"received $p")
      stay using b.copy(lastPacket = System.currentTimeMillis(), sending = b.sending.filter {
        case Right(p) if p.message_identifier == p.message_identifier => true
        case _ => false
      })

    case Event(p: Pingreq, b: SessionConnectedBag) =>
      log.debug(s"Got Pingreq $p and bag => $b")
      sender ! Pingresp(Header(dup = false, 0, retain = false))
      stay using b.copy(lastPacket = System.currentTimeMillis)

    case Event(pp @ PublishPayload(Publish(header, topic, _, payload), _, _, _), b: SessionConnectedBag) =>
      log.info(s"[${context.self.path}] got PublishPayload $pp")
      val qos = header.qos min pp.qos
      val publish = Publish(Header(header.dup, qos, pp.auto), topic, if (qos == 0) 0 else b.messageId, payload)
      metricsActor.foreach(_ ! TrackablePublish(pp.id, publish, new DateTime()))
      b.connection ! publish
      if (qos > 0) {
        log.info(s"[${context.self.path}] incrementing message id to " + (b.messageId + 1) + s" for qos $qos")
        stay using b.copy(messageId = b.messageId + 1, sending = b.sending :+ Left(publish))
      } else {
        stay
      }

    case Event(GetSessionInfo, b: SessionConnectedBag) =>
      sender() ! b
      stay()
  }

  whenUnhandled {
    case Event(ResetSession, b) =>
      log.debug(s"[${context.self.path.name}] Resetting state with $b")
      if (b.cleanSession) bus ! BusDetach(self)
      goto(WaitingForNewSession) using SessionWaitingBag(b.sending, List(), b.subs, 1, b.cleanSession)

    case Event(_: Disconnect, b: SessionConnectedBag) =>
      log.info(s"[${context.self.path.name}] Disconnecting with $b")
      b.entity match {
        case _: Device =>
          goto(WaitingForNewSession) using SessionWaitingBag(b.sending, List(), b.subs, 1, b.cleanSession)
        case _ =>
          bus ! BusDetach(self)
          stop(FSM.Shutdown)
      }
    case Event(_: Disconnect, b: SessionWaitingBag) =>
      log.error(s"[${context.self.path.name}] unexpected Disconnect for $stateName with $b")
      stay()

    case Event(e, b) =>
      log.error(s"[${context.self.path.name}] unexpected message $e for $stateName with $b")
      goto(WaitingForNewSession) using SessionWaitingBag(b.sending, List(), List(), 1, b.cleanSession)
  }

  onTermination {
    case StopEvent(e, s, d) =>
      log.debug(s"[${context.self.path.name}] Terminated with $e and $s and $d")
  }

  onTransition {
    case (from: SessionState, to: SessionState) =>
      log.debug(s"[${context.self.path.name}] State changed from $from to $to")
  }

  initialize()
}

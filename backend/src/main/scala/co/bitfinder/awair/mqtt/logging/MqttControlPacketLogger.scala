package co.bitfinder.awair.mqtt.logging

import co.bitfinder.awair.Entity
import co.bitfinder.mqtt.{Connect, Packet, Subscribe}
import org.slf4j.LoggerFactory

/**
  * Created by simonkim on 6/21/17.
  */
object MqttControlPacketLogger {

  private val log = LoggerFactory.getLogger("MqttControlPacketLogger")

  private def logLatency(entity: Entity, clientId: String, packet: Packet): Unit = {
    val latencyO = LatencyTracker.getLatencyAndRemove(clientId, packet)
    latencyO.foreach { latency =>
      log.info(
        s"$entity ${packet.getClass.getSimpleName} $latency",
        "type" -> "mqtt-ack-latency",
        "entity" -> entity.getClass.getSimpleName,
        "id" -> entity.toString,
        "ms" -> latency,
        "ack" -> packet.getClass.getSimpleName)
    }
  }

  def logInbound(entity: Entity, clientId: String, packet: Packet): Unit = {
    log.debug(s"$entity sent $packet")
    packet match {
      case _: Connect => LatencyTracker.register(clientId, packet)
      case _: Subscribe => LatencyTracker.register(clientId, packet)
      case _ =>
    }
  }

  def logOutbound(entity: Entity, clientId: String, packet: Packet): Unit = {
    log.debug(s"$entity is sent $packet")
    packet match {
      case _: Connect => logLatency(entity, clientId, packet)
      case _: Subscribe => logLatency(entity, clientId, packet)
      case _ =>
    }
  }

  def logDisconnection(entity: Entity): Unit = {
    log.info(
      s"$entity disconnected",
      "type" -> "mqtt-disconnection",
      "entity" -> entity.getClass.getSimpleName,
      "id" -> entity.toString)
  }

}

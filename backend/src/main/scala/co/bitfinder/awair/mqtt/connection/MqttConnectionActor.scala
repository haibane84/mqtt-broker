package co.bitfinder.awair.mqtt.connection

import java.util.UUID

import akka.actor.{ActorRef, FSM}
import akka.io.Tcp.PeerClosed
import co.bitfinder.awair.Entity
import co.bitfinder.awair.mqtt.AwairTcpConnectionActor.{Closing, ReceivedPacket, SendingPacket}
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor.{PublishWithEntity, _}
import co.bitfinder.awair.mqtt.logging.MqttControlPacketLogger
import co.bitfinder.awair.mqtt.session.SessionActor.{ConnectionLost, KeepAliveTimeout, WrongState}
import co.bitfinder.mqtt._
import org.joda.time.DateTime
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor.{PublishWithEntity, _}
import kamon.Kamon
import kamon.metric.instrument.Counter

import scala.concurrent.duration._
import scala.language.postfixOps

object MqttConnectionActor {
  sealed trait ConnectionState

  case object Active extends ConnectionState
  case object Waiting extends ConnectionState

  sealed trait ConnectionBag
  case class EmptyConnectionBag() extends ConnectionBag
  case class EmptySessionBag(packet: ConnectWithEntity, connection: ActorRef) extends ConnectionBag
  case class ConnectionSessionBag(session: ActorRef, connection: ActorRef) extends ConnectionBag

  case class TrackablePublish(id: String, msg: Publish, date: DateTime)
  case class ConnectWithEntity(connect: Connect, entity: Entity)
  case class SubscribeWithEntity(subscribe: Subscribe, entity: Entity)
  case class PublishWithEntity(publish: Publish, entity: Entity, clientId: String)
  case class DisconnectWithEntity(disconnect: Disconnect, entity: Entity, clientId: String)
  case object GetCurrentState
}

class MqttConnectionActor(sessionManager: ActorRef, metricsActor: Option[ActorRef]) extends FSM[ConnectionState, ConnectionBag] {
  val sessionTimeoutCounter: Counter = Kamon.metrics.counter("session-timeout-counter")

  startWith(Waiting, EmptyConnectionBag())

  when(Active) {
    case Event(p: Packet, bag: ConnectionSessionBag) =>
      bag.connection ! SendingPacket(p)
      stay

    case Event(ReceivedPacket(c: Connect, entityO, _), bag: ConnectionSessionBag) =>
      log.warning(s"[${context.self.path.name}] Unexpected Connect. Closing peer")
      bag.session ! Disconnect(Header(dup = false, qos = 0, retain = false))
      bag.connection ! Closing
      stay

    case Event(ReceivedPacket(dis: Disconnect, entityO, clientId), bag: ConnectionSessionBag) =>
      log.info(s"[${context.self.path.name}] Disconnect. Closing peer")
      bag.session ! dis
      bag.connection ! Closing
      if (entityO.nonEmpty && clientId.nonEmpty) sessionManager ! DisconnectWithEntity(dis, entityO.get, clientId.get)
      stay

    case Event(ReceivedPacket(pub: Publish, entityO, clientId), bag: ConnectionSessionBag) =>
      val t = TrackablePublish(UUID.randomUUID().toString, pub, new DateTime())
      bag.session ! t
      metricsActor.foreach(_ ! t)
      if (entityO.nonEmpty && clientId.nonEmpty) sessionManager ! PublishWithEntity(pub, entityO.get, clientId.get)
      stay

    case Event(ReceivedPacket(sub: Subscribe, Some(entity), _), bag: ConnectionSessionBag) =>
      bag.session ! SubscribeWithEntity(sub, entity)
      stay

    case Event(ReceivedPacket(p: Packet, _, _), bag: ConnectionSessionBag) =>
      bag.session ! p
      stay

    case Event(WrongState, b: ConnectionSessionBag) =>
      log.warning(s"[${context.self.path.name}] Session was in a wrong state")
      b.connection ! Closing
      stay

    case Event(KeepAliveTimeout, b: ConnectionSessionBag) =>
      log.info("Keep alive timed out. Closing connection")
      b.connection ! Closing
      sessionTimeoutCounter.increment()
      stay

    case Event(PeerClosed, b: ConnectionSessionBag) =>
      b.session ! ConnectionLost
      sender() ! "closedAck"
      stop(FSM.Shutdown)
  }

  when(Waiting) {
    case Event(ReceivedPacket(c: Connect, Some(e), _), _) =>
      sessionManager.tell(ConnectWithEntity(c, e), self)
      stay() using EmptySessionBag(ConnectWithEntity(c, e), sender) forMax 3.second

    case Event(session: ActorRef, b: EmptySessionBag) =>
      session ! b.packet
      log.debug(s"[${context.self.path.name}] Got Session and Creating ${ConnectionSessionBag(session, sender)}")
      goto(Active) using ConnectionSessionBag(session, b.connection)

    case Event(StateTimeout, b: EmptySessionBag) =>
      log.warning(s"Got StateTimeout for $b so Closing peer")
      b.connection ! Closing
      stay

    case Event(PeerClosed, _) =>
      sender() ! "closedAck"
      stop(FSM.Shutdown)

    case x =>
      log.warning(s"[${context.self.path.name}] unexpected " + x + " for waiting. Closing peer")
      sender ! Closing
      stay
  }

  whenUnhandled {
    case Event(StateTimeout, _) =>
      log.error(s"[${context.self.path.name}] unexpected StateTimeout. Closing peer")
      sender ! Closing
      stop(FSM.Shutdown)

    case Event(GetCurrentState, c) =>
      sender ! stateName
      stay

    case Event(x, _) =>
      log.error("unexpected " + x + " for " + stateName + ". Closing peer")
      sender ! Closing
      stay
  }

  onTermination {
    case StopEvent(x, s, d) =>
      log.debug("Terminated with " + x + " and " + s + " and " + d)
  }

  onTransition(handler _)

  def handler(from: ConnectionState, to: ConnectionState): Unit = {
    log.debug("State changed from " + from + " to " + to)
  }

  initialize()
}

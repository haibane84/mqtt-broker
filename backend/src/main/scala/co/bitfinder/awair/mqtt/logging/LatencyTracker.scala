package co.bitfinder.awair.mqtt.logging

import co.bitfinder.mqtt.{Connect, Packet, Subscribe}
import org.joda.time.DateTime

import scala.collection.mutable

/**
  * Created by simonkim on 6/20/17.
  */
object LatencyTracker {

  val packetArrivalTimes: mutable.Map[String, Long] = mutable.Map[String, Long]()

  implicit class HashableClientIdAndPacket(val v: (String, Packet)) {
    val (clientId, packet) = v
    def toUniqueHash: Option[String] = packet match {
      case c: Connect => Some(s"${c.getClass.getSimpleName}-$clientId")
      case s: Subscribe => Some(s"${s.getClass.getSimpleName}-$clientId")
      case _ => None
    }
  }

  def register(clientId: String, packet: Packet): Unit = {
    (clientId, packet).toUniqueHash match {
      case Some(key) => packetArrivalTimes.update(key, new DateTime().getMillis)
      case None =>
    }
  }

  def getLatencyAndRemove(clientId: String, packet: Packet): Option[Long] = {
    (clientId, packet).toUniqueHash.flatMap { key =>
      val latencyO = packetArrivalTimes.get(key) match {
        case Some(arrivalTime) => Some(new DateTime().getMillis - arrivalTime)
        case None => None
      }
      packetArrivalTimes -= key
      latencyO
    }
  }

}

package co.bitfinder.awair.mqtt.connection

import akka.actor.{Actor, ActorLogging, ActorRef}


case class BusSubscribe(topic: String, actor: ActorRef, qos: Int = 0)
case class BusUnsubscribe(topic: String, actor: ActorRef)


case class BusPublish(topic: String, payload: Any, retain: Boolean = false, clean_retain: Boolean = false, id: String = "")

case class BusDetach(actor: ActorRef)

case class PublishPayload(payload: Any, auto: Boolean = false, qos: Int = 0, id: String = "")

case class Subscription(topic: String, session: ActorRef, qos: Int)

case class SubInfo(topic: String, sessionUrl: String, qos: Int)

case class Retain(topic: String, payload: Any)

case object GetBusInfo

class EventBusActor extends Actor with ActorLogging {

  context become working(List(), List())

  def working(subscriptions: List[Subscription], retains: List[Retain]): Receive = {
    case p: BusSubscribe =>
      log.info("subscribe " + p)
      MqttTopicClassifier.checkTopicName(p.topic)

      val similar = subscriptions.filter(s => (s.topic == p.topic) && (s.session == p.actor)).toArray
      if (similar.length == 0) {
        context become working(Subscription(p.topic, p.actor, p.qos) :: subscriptions, retains)
        log.debug("subscribed to " + p.topic)
      } else {
        log.debug("subscription already exists " + p)
        log.debug("changing existing subscription qos to " + p.qos)
        context become working(Subscription(p.topic, p.actor, p.qos) :: subscriptions.filter(t => !(t.topic == p.topic && t.session == p.actor)), retains)
      }
      retains
        .filter(r => MqttTopicClassifier.isSubclass(r.topic, p.topic))
        .sortBy(_.topic)
        .foreach(r => p.actor ! PublishPayload(r.payload, auto = true, qos = p.qos))

    case p: BusUnsubscribe =>
      log.info("unsubscribe " + p)
      context become working(subscriptions.filter(s => !(s.topic == p.topic && s.session == p.actor)), retains)

    case p: BusDetach =>
      log.info(s"clearing bus for $p and ${subscriptions.size}")
      context become working(subscriptions.filter(s => s.session != p.actor), retains)

    case p: BusPublish =>
      log.debug("buspublish  " + p)
      log.debug("current subscriptions " + subscriptions.map(s => s.topic + "@" + s.qos + "->" + s.session).mkString(", "))
      subscriptions
        .filter(s => MqttTopicClassifier.isSubclass(p.topic, s.topic))
        .groupBy(_.session)
        .map(t => (t._1, t._2.toArray))
        .foreach(t => {
          log.debug("publish " + p + " by subscriptions: " + t._2.map(x => x.topic + "@" + x.qos + "->" + x.session).mkString(", "))
          val maxQos = t._2.map(_.qos).max
          t._1 ! PublishPayload(p.payload, auto = false, qos = maxQos, p.id)
        })
      if (p.retain) {
        if (p.clean_retain) {
          log.debug("cleaning retain for " + p.topic)
          context become working(subscriptions, retains.filter(_.topic != p.topic))
        } else {
          context become working(subscriptions, Retain(p.topic, p.payload) :: retains.filter(_.topic != p.topic))
        }
      }

    case GetBusInfo =>
      sender() ! (subscriptions.map(d => SubInfo(d.topic ,d.session.path.toString, d.qos)), retains)

  }

  def receive: Receive = {
    case x => log.error("It was unexcepted " + x)
  }
}

object MqttTopicClassifier {
  def checkTopicName(to: String): Boolean = {
    if (to != "#" && to.contains("#") &&
      (to.replace("#", "/#") != to.replace("/#", "//#") || to.last != '#')) {
      throw new BadSubscriptionException(to)
    }
    if (to != "+" && to.contains("+") && (to.charAt(0) != '+') && to.replace("+", "/+") != to.replace("/+", "//+")) {
      throw new BadSubscriptionException(to)
    }
    if (to.length > 0 && to.charAt(0) == '+') checkTopicName(to.substring(1))
    true
  }

  def isSubclass(actual: String, subscribing: String): Boolean = {
    if (!subscribing.contains('#') && !subscribing.contains('+')) return isPlainSubclass(actual, subscribing)
    if (subscribing == "#") return true
    val squareIndex = subscribing.indexOf('#')
    if (squareIndex > 0) {
      val sub = if (squareIndex > 1) subscribing.substring(0, squareIndex - 1) + ".*" else "/.*"
      return isRegexSubclass(actual, sub)
    }
    isRegexSubclass(actual, subscribing)
  }

  private def isPlainSubclass(actual: String, subscribing: String): Boolean =
    subscribing == actual

  private def isRegexSubclass(actual: String, subscribing: String): Boolean = {
    val reg = subscribing.zipWithIndex.map {
      case (c, i) =>
        if (c == '+')
          if (i == 0 || i == (subscribing.length - 1)) "[^/]*" else "[^/]+"
        else
          c.toString
    }.mkString
    val res = actual.matches(reg)
    res
  }

  private def isSquaredSubclass(actual: String, sub: String) = {
    actual.startsWith(sub)
  }
}

class BadSubscriptionException(msg: String) extends Throwable

package co.bitfinder.awair.mqtt

import akka.actor.{Actor, ActorRef}
import akka.io.Tcp.Write
import co.bitfinder.awair.mqtt.AwairTcpConnectionActor.Ack


/**
  * Created by simonkim on 3/16/17.
  */
class MockTcpConnectionActor(originalSender: ActorRef) extends Actor {
  override def receive: Receive = {
    case s: Write =>
      sender() ! Ack
      originalSender ! s.data
    case any =>
      originalSender ! any
  }
}

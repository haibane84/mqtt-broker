package co.bitfinder.awair.mqtt

import akka.actor.{ActorNotFound, ActorRef, ActorSystem, Props}
import akka.io.Tcp
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import akka.util.ByteString
import co.bitfinder.awair.mqtt.connection.EventBusActor
import co.bitfinder.awair.mqtt.session.{SessionManagerSupervisor, SessionsManagerActor}
import co.bitfinder.mqtt.Helpers._
import co.bitfinder.mqtt.{Header, _}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfter, FlatSpecLike, Matchers}
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, Results}
import play.api.routing.sird._
import play.api.test.WsTestClient
import play.core.server.Server

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class IntegrationSpec extends TestKit(ActorSystem("IntegrationSpec")) with ImplicitSender with FlatSpecLike with Matchers with BeforeAndAfter {

  private implicit val materializer = ActorMaterializer()(system)
  private implicit val actorRef = testActor

  private val userId = 0
  private val deviceType = "awair"
  private val deviceId = 1
  private val clientId = "clientId1"

  private val dummyActor = TestProbe().ref
  private val eventTopic = s"org/559b45ef956608eb31b05db9/device/awair/$deviceId/event/sensor/format/json"
  private val commandTopic = s"org/559b45ef956608eb31b05db9/device/awair/$deviceId/command/switch/format/json"
  private val deviceToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdfaWQiOiI1NTliNDVlZjk1NjYwOGViMzFiMDVkYjkiLCJkZXZpY2VfdHlwZSI6ImF3YWlyIiwiZGV2aWNlX2lkIjoiMSIsImlhdCI6MTQ1ODUzNDU4Nn0.rnykqVgw_UeoPnm03aj30hZVHdifD2b6gB6ADxcwdLg"
  private val userToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdfaWQiOiI1NTliNDVlZjk1NjYwOGViMzFiMDVkYjkiLCJ1c2VyX2lkIjoiMCJ9.xh8oJhxP2E-Df_s446QkLSK7EySuqxOHVonD7aXHpJc"
  private val orgToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdfaWQiOiI1NTliNDVlZjk1NjYwOGViMzFiMDVkYjkifQ.4uZsGKhyZ75LXlMwf5oNWM8JSWdIM3VhVB3QkVeUmE8"

  private val bus = system.actorOf(Props[EventBusActor], "bus")
  private val sessionManagerSupervisor = system.actorOf(SessionManagerSupervisor.props, "sessionManagerSupervisor")
  system.actorOf(Props(new SessionsManagerActor(bus, sessionManagerSupervisor, None)), "sessionManager1")
  system.actorOf(Props(new SessionsManagerActor(bus, sessionManagerSupervisor, None)), "sessionManager2")

  private def withTcpConn(block: ActorRef => Unit)(implicit destroyAfterUse: Boolean = true) = Server.withRouter() {
    case GET(p"/v1/admin-ownerships/$deviceType/$deviceId") => Action { Results.Ok(Json.obj("user_id" -> userId)) }
  } { implicit port =>
    WsTestClient.withClient { client =>
      val mockConnectionActor = system.actorOf(Props(new MockTcpConnectionActor(testActor)))
      val h = system.actorOf(Props(new AwairTcpConnectionActor(sessionManagerSupervisor, dummyActor, client, mockConnectionActor, None)).withMailbox("priority-dispatcher"), "testActor")
      block(h)
      if (destroyAfterUse) {
        h ! Disconnect(Header()).toTcpReceived
        expectMsg(Tcp.Close)
      }
    }
  }

  private def withTwoConns[T](token1: String, token2: String)(block: (ActorRef, ActorRef) => T): T = withHttpClient { implicit client =>
    block(createConn(token1), createConn(token2))
  }

  private def withHttpClient[T](block: WSClient => T): T = Server.withRouter() {
    case GET(p"/v1/admin-ownerships/$deviceType/$deviceId") => Action { Results.Ok(Json.obj("user_id" -> userId)) }
  } { implicit port =>
    WsTestClient.withClient { client => block(client) }
  }

  private def createConn(token: String, clientId: String = "", willTopic: Option[String] = None, willMessage: Option[String] = None, keepAlive: Int = 0, cleanSession: Boolean = true)(implicit client: WSClient): ActorRef = {
    val mockConn = system.actorOf(Props(new MockTcpConnectionActor(testActor)))
    val conn = system.actorOf(AwairTcpConnectionActor.props(sessionManagerSupervisor, dummyActor, client, mockConn, None).withMailbox("priority-dispatcher"))
    (willTopic, willMessage) match {
      case (Some(t), Some(m)) =>
        conn ! Connect(Header(), ConnectFlags(true, false, false, 0, true, cleanSession, keepAlive), clientId, Some(t), Some(m), Some(token), None).toTcpReceived
      case _ =>
        conn ! Connect(Header(), ConnectFlags(true, false, false, 0, false, cleanSession, keepAlive), clientId, None, None, Some(token), None).toTcpReceived
    }
    expectMsg(Connack(Header(),0).toByteString)
    conn
  }

  private def disconnectConns(conns: ActorRef*): Unit = {
    conns.toList.map { c =>
      c ! Disconnect(Header()).toTcpReceived
      expectMsg(Tcp.Close)
    }
  }

  "MQTT broker" should "handle connect message and send back connack" in withTcpConn { c =>
    val connect = Connect(Header(), ConnectFlags(true, false, false, 0, false, true, 0), clientId, None, None, Some(deviceToken), None)
    c ! connect.toTcpReceived
    expectMsg(Connack(Header(), 0).toByteString)
  }

  it should "handle subscribe req and send back suback" in withTcpConn { c =>
    val connect = Connect(Header(), ConnectFlags(true, false, false, 0, false, true, 0), clientId, None, None, Some(userToken), None)
    c ! connect.toTcpReceived
    expectMsg(Connack(Header(), 0).toByteString)

    val subscribe = Subscribe(Header(), 2, Vector((eventTopic, 0)))
    c ! subscribe.toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)
  }

  it should "close when it gets broken package" in {
    implicit val disconnectAfterUse = false
    withTcpConn { c =>
      c ! "broken".toTcpReceived
      expectMsg(Tcp.Close)
    }
  }

  it should "always send publish message with QoS 0 regardless of pub and sub QoS" in withTwoConns(deviceToken, userToken) { (d, a) =>
    a ! Subscribe(Header(), 2, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    d ! Publish(Header(false, 1, false), eventTopic, 0, "{\"score\":32}").toTcpReceived
    expectMsg(Publish(Header(false, 0, false), eventTopic, 0, "{\"score\":32}").toByteString)


    a ! Subscribe(Header(), 0, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 0, Vector(0)).toByteString)

    d ! Publish(Header(false, 1, false), eventTopic, 0, "{\"score\":32}").toTcpReceived
    expectMsg(Publish(Header(false, 0, false), eventTopic, 0, "{\"score\":32}").toByteString)

    disconnectConns(d, a)
  }

  it should "deliver event published by device to subscribing app with qos 0" in withTwoConns(deviceToken, userToken) { (d, a) =>
    a ! Subscribe(Header(), 2, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    a ! Subscribe(Header(), 2, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    d ! Publish(Header(), eventTopic, 0, "{\"score\":32}").toTcpReceived
    expectMsg(Publish(Header(), eventTopic, 0, "{\"score\":32}").toByteString)
    expectNoMsg

    disconnectConns(d, a)
  }

  it should "deliver device event to app for the subscription that it made for a device that was previously connected but now is offline" in withHttpClient { implicit client =>
    val d = createConn(deviceToken)
    disconnectConns(d)

    val a = createConn(userToken)
    a ! Subscribe(Header(), 2, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    val d2 = createConn(deviceToken)
    d2 ! Publish(Header(), eventTopic, 0, "{\"score\":33}").toTcpReceived
    expectMsg(Publish(Header(), eventTopic, 0, "{\"score\":33}").toByteString)
    disconnectConns(d2, a)
  }

  it should "deliver device event to app for the subscription that was made for a device that never had connection with broker but now is connected" in withHttpClient { implicit client =>
    val a = createConn(userToken)

    a ! Subscribe(Header(), 2, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    val d = createConn(deviceToken)

    d ! Publish(Header(), eventTopic, 0, "{\"score\":32}").toTcpReceived
    expectMsg(Publish(Header(), eventTopic, 0, "{\"score\":32}").toByteString)

    disconnectConns(d, a)
  }

  it should "deliver device command from user to subscribing device with qos 0" in withTwoConns(deviceToken, userToken) { (d, a) =>
    d ! Subscribe(Header(), 2, Vector((commandTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    a ! Publish(Header(), commandTopic, 0, "{\"switch\":on}").toTcpReceived
    expectMsg(Publish(Header(), commandTopic, 0, "{\"switch\":on}").toByteString)

    disconnectConns(d, a)
  }

  it should "deliver device event message published by an org to subscribing app with qos 0" in withTwoConns(orgToken, userToken) { (o, a) =>
    a ! Subscribe(Header(), 2, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    o ! Publish(Header(), eventTopic, 0, "{\"score\":32}").toTcpReceived
    expectMsg(Publish(Header(), eventTopic, 0, "{\"score\":32}").toByteString)

    disconnectConns(o, a)
  }

  it should "deliver device command message published by an org to subscribing device with qos 0" in withTwoConns(orgToken, deviceToken) { (o, d) =>
    d ! Subscribe(Header(), 2, Vector((commandTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    o ! Publish(Header(), commandTopic, 0, "{\"switch\":true}").toTcpReceived
    expectMsg(Publish(Header(), commandTopic, 0, "{\"switch\":true}").toByteString)

    disconnectConns(o, d)
  }

  it should "deliver device event published by a device to subscribing org with qos 0" in withTwoConns(orgToken, deviceToken) { (o, d) =>
    o ! Subscribe(Header(), 2, Vector((eventTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(),2,Vector(0)).toByteString)

    d ! Publish(Header(), eventTopic, 0, "{\"score\":10}").toTcpReceived
    expectMsg(Publish(Header(), eventTopic, 0, "{\"score\":10}").toByteString)

    disconnectConns(o, d)
  }

  it should "support org with subscribing wild card topics" in withTwoConns(orgToken, deviceToken) { (o, d) =>
    val wildCardTopic = "org/559b45ef956608eb31b05db9/device/awair/+/event/sensor/format/json"

    o ! Subscribe(Header(), 2, Vector((wildCardTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(),2,Vector(0)).toByteString)

    d ! Publish(Header(), eventTopic, 0, "{\"score\":10}").toTcpReceived
    expectMsg(Publish(Header(), eventTopic, 0, "{\"score\":10}").toByteString)

    disconnectConns(o, d)
  }

  it should "support Keep Alive" in withHttpClient { implicit c =>
    createConn(deviceToken, keepAlive = 1)
    awaitAssert({
      expectMsg(Tcp.Close)
    }, 2.seconds)
  }

  it should "publish last will message to last will topic when server closes the Network Connection because of a protocol error" in withHttpClient { implicit client =>
    val max = 3.seconds
    val willTopic = s"org/559b45ef956608eb31b05db9/device/awair/$deviceId/event/network/format/json"
    val willMessage = "{\"connection\":\"disconnected\"}"

    val d = createConn(deviceToken, willTopic = Some(willTopic), willMessage = Some(willMessage))
    val a = createConn(userToken)

    a ! Subscribe(Header(), 2, Vector((willTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    d ! "broken".toTcpReceived

    // The order of Tcp.close and Will Publish is not guaranteed
    1 to 2 foreach { _ =>
      expectMsgPF(5.seconds) {
        case Tcp.Close =>
        case msg: ByteString if msg == Publish(Header(), willTopic, 0, willMessage).toByteString =>
      }
    }

    disconnectConns(a)
  }

  it should "publish last will message to last will topic when client fails to communicate within the Keep Alive time" in withHttpClient { implicit client =>
    val willTopic = s"org/559b45ef956608eb31b05db9/device/awair/$deviceId/event/network/format/json"
    val willMessage = "{\"connection\":\"disconnected\"}"

    val d = createConn(deviceToken, willTopic = Some(willTopic), willMessage = Some(willMessage), keepAlive = 1)
    val a = createConn(userToken)

    a ! Subscribe(Header(), 2, Vector((willTopic, 0))).toTcpReceived
    expectMsg(Suback(Header(), 2, Vector(0)).toByteString)

    awaitAssert({
      expectMsg(Tcp.Close)
    }, 2.seconds)

    expectMsg(Publish(Header(), willTopic, 0, willMessage).toByteString)

    disconnectConns(a)
  }

  it should "not support qos 1 subscribe and change the qos to 0" in withHttpClient { implicit c =>
    val d = createConn(deviceToken)
    val qosRequested = 1
    val subscribe = Subscribe(Header(), 2, Vector((s"org/559b45ef956608eb31b05db9/device/awair/$deviceId/event/sensor/format/json", qosRequested)))
    d ! subscribe.toTcpReceived
    val qosResponded = 0
    expectMsg(Suback(Header(), 2, Vector(qosResponded)).toByteString)
    disconnectConns(d)
  }

  it should "destroy User and Org sessions after connection is closed regardless of the `clean_session` value" in withHttpClient { implicit c =>
    def getUserSessionF = system.actorSelection(s"/user/sessionManager*/user-$userId-clientId").resolveOne(50.milliseconds)
    val a = createConn(userToken, clientId = "clientId", cleanSession = false)
    Await.result(getUserSessionF, 3.seconds) shouldBe an [ActorRef]
    disconnectConns(a)
    ScalaFutures.whenReady(getUserSessionF.failed) { es =>
      es shouldBe an [ActorNotFound]
    }

    def getOrgSessionF = system.actorSelection(s"/user/sessionManager*/org-clientId").resolveOne(50.milliseconds)
    val o = createConn(orgToken, clientId = "clientId", cleanSession = false)
    Await.result(getOrgSessionF, 3.seconds) shouldBe an [ActorRef]
    disconnectConns(o)
    ScalaFutures.whenReady(getOrgSessionF.failed) { es =>
      es shouldBe an [ActorNotFound]
    }
  }

  it should "not destroy Device session even after connection is closed regardless of the `clean_session` value" in withHttpClient { implicit c =>
    val d = createConn(deviceToken, cleanSession = true)
    disconnectConns(d)
    val sessionF: Future[ActorRef] = system.actorSelection(s"/user/sessionManager*/device-awair-$deviceId").resolveOne(50.milliseconds)
    ScalaFutures.whenReady(sessionF) { actorRef =>
      actorRef.path.name shouldBe s"device-awair-$deviceId"
    }
  }
}

package co.bitfinder.awair.mqtt.logging

import co.bitfinder.mqtt.{Connect, ConnectFlags, Header}
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}
import LatencyTracker._

/**
  * Created by simonkim on 6/20/17.
  */
class LatencyTrackerSpec extends FlatSpec with Matchers {

  private def packet = Connect(Header(true, 0, false), ConnectFlags(true, true, false, 0, false, true, 1), "clientid")

  private val clientId = "CLIENT_ID"

  "register" should "keep the arrival time of the control packet internally and uniquely" in {
    LatencyTracker.register(clientId, packet)
    LatencyTracker.packetArrivalTimes((clientId, packet).toUniqueHash.get) should be > new DateTime().minusSeconds(1).getMillis

    LatencyTracker.register(clientId, packet)
    LatencyTracker.packetArrivalTimes.keys.size shouldBe 1
  }

  "getLatencyAndRemove" should "get latency in millisecond between current time and arrival time of control packet and remove the control packet from latency tracker" in {
    LatencyTracker.packetArrivalTimes((clientId, packet).toUniqueHash.get) = new DateTime().getMillis
    val latencyO = LatencyTracker.getLatencyAndRemove(clientId, packet)
    latencyO.get should be >= 0L
    LatencyTracker.packetArrivalTimes.get((clientId, packet).toUniqueHash.get) shouldBe None
  }

}
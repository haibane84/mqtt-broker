package co.bitfinder.awair.mqtt.session

import akka.actor.{ActorRef, ActorSystem, PoisonPill}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import co.bitfinder.awair.Device
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor.ConnectWithEntity
import co.bitfinder.awair.mqtt.session.SessionManagerSupervisor.GetDeviceSession
import co.bitfinder.mqtt.{Connect, ConnectFlags, Header}
import org.scalatest.{BeforeAndAfter, FlatSpecLike, Matchers}

import scala.concurrent.duration._

/**
  * Created by jeado on 2017. 4. 4..
  */
class SessionManagerSupervisorSpec extends TestKit(ActorSystem("Test")) with ImplicitSender with FlatSpecLike with Matchers with BeforeAndAfter {
  implicit val timeout: FiniteDuration = 1.seconds

  private val deviceId = 1
  private val otherDeviceId = 2

  var sessionManagerSupervisor: ActorRef = _
  var sessionManager1: ActorRef = _
  var sessionManager2: ActorRef = _
  val probe = TestProbe()

  before {
    sessionManagerSupervisor = system.actorOf(SessionManagerSupervisor.props, "sessionManagerSupervisor")
    sessionManager1 = system.actorOf(SessionsManagerActor.props(testActor, sessionManagerSupervisor, None), "sessionManager1")
    sessionManager2 = system.actorOf(SessionsManagerActor.props(testActor, sessionManagerSupervisor, None), "sessionManager2")

    probe watch sessionManagerSupervisor
    probe watch sessionManager1
    probe watch sessionManager2
  }

  after {
    sessionManager1 ! PoisonPill
    probe.expectTerminated(sessionManager1)
    sessionManager2 ! PoisonPill
    probe.expectTerminated(sessionManager2)
    sessionManagerSupervisor ! PoisonPill
    probe.expectTerminated(sessionManagerSupervisor)
  }

  it should "create session name by the pattern of device-$deviceType-$deviceId from entity" in {
    val connect1 = Connect(Header(), ConnectFlags(true, false, false, 0, false, true, 0), "clientId1", None, None, None, None)
    val connect2 = Connect(Header(), ConnectFlags(true, false, false, 0, false, true, 0), "clientId2", None, None, None, None)

    sessionManagerSupervisor ! ConnectWithEntity(connect1, Device("awair", deviceId))
    expectMsgPF(timeout) {
      case actor: ActorRef => actor.path.name shouldBe "device-awair-1"
    }

    sessionManagerSupervisor ! ConnectWithEntity(connect2, Device("awair", otherDeviceId))
    expectMsgPF(timeout) {
      case actor: ActorRef => actor.path.name shouldBe "device-awair-2"
    }
  }

  it should s"return session corresponding to device entity" in {
    val connect1 = Connect(Header(), ConnectFlags(true, false, false, 0, false, true, 0), "clientId1", None, None, None, None)
    val connect2 = Connect(Header(), ConnectFlags(true, false, false, 0, false, true, 0), "clientId2", None, None, None, None)
    sessionManagerSupervisor ! ConnectWithEntity(connect1, Device("awair", deviceId))
    sessionManagerSupervisor ! ConnectWithEntity(connect2, Device("awair", otherDeviceId))

    expectMsgType[ActorRef]
    expectMsgType[ActorRef]

    sessionManagerSupervisor ! GetDeviceSession(Device("awair", otherDeviceId))
    expectMsgPF(timeout) {
      case actor: ActorRef => actor.path.name shouldBe "device-awair-2"
    }
  }
}

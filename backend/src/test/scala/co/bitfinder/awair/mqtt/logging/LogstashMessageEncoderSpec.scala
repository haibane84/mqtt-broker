package co.bitfinder.awair.mqtt.logging

import ch.qos.logback.classic.{Level, LoggerContext}
import ch.qos.logback.classic.spi.LoggingEvent
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import play.api.libs.json.{JsSuccess, Json, Reads}

/**
  * Created by jay on 12/2/16.
  */
class LogstashMessageEncoderSpec extends FlatSpec with BeforeAndAfterEach with Matchers {

  implicit val dateTimeReads: Reads[DateTime] = Reads(js => js.validate[String].flatMap(s => JsSuccess(new DateTime(s))))
  private val logger = new LoggerContext().getLogger(classOf[LogstashMessageEncoderSpec])
  private val encoder = new LogstashMessageEncoder

  "LogstashMessageEncoder" should "encode an ERROR log" in {
    val th = new Exception("exception")
    val evt = new LoggingEvent("", logger, Level.ERROR, "message", th, Array())
    val json = Json.parse(encoder.doEncode(evt))
    (json \ "@version").as[String] shouldBe "1"
    (json \ "@timestamp").as[DateTime].getMillis should be (System.currentTimeMillis() +- 1000)
    (json \ "level").as[String] shouldBe "ERROR"
    (json \ "type").as[String] shouldBe "awair-messaging-service-log"
    (json \ "tags").as[List[String]] shouldBe List("awair-mqtt-broker")
    (json \ "message").as[String].split("\n").take(3) shouldBe Array(
      "message",
      "java.lang.Exception: exception",
      "\tat co.bitfinder.awair.mqtt.logging.LogstashMessageEncoderSpec$$anonfun$1.apply$mcV$sp(LogstashMessageEncoderSpec.scala:19)")
    (json \ "thread").as[String].matches(".+-running-LogstashMessageEncoderSpec") shouldBe true
  }

  it should "encode an WARN log" in {
    val evt = new LoggingEvent("", logger, Level.WARN, "message", null, Array())
    val json = Json.parse(encoder.doEncode(evt))
    (json \ "@version").as[String] shouldBe "1"
    (json \ "@timestamp").as[DateTime].getMillis should be (System.currentTimeMillis() +- 1000)
    (json \ "level").as[String] shouldBe "WARN"
    (json \ "message").as[String] shouldBe "message"
    (json \ "type").as[String] shouldBe "awair-messaging-service-log"
    (json \ "tags").as[List[String]] shouldBe List("awair-mqtt-broker")
    (json \ "thread").as[String].matches(".+-running-LogstashMessageEncoderSpec") shouldBe true
  }

  it should "encode an INFO log with additional JSON in arguments array" in {
    val evt = new LoggingEvent("", logger, Level.INFO, "message", null, Array("number" -> 0.1, "string" -> "string", "type" -> "custom-log"))
    val json = Json.parse(encoder.doEncode(evt))
    (json \ "@version").as[String] shouldBe "1"
    (json \ "@timestamp").as[DateTime].getMillis should be (System.currentTimeMillis() +- 1000)
    (json \ "level").as[String] shouldBe "INFO"
    (json \ "type").as[String] shouldBe "custom-log"
    (json \ "tags").as[List[String]] shouldBe List("awair-mqtt-broker")
    (json \ "message").as[String] shouldBe "message"
    (json \ "thread").as[String].matches(".+-running-LogstashMessageEncoderSpec") shouldBe true
    (json \ "number").as[Double] shouldBe 0.1
    (json \ "string").as[String] shouldBe "string"
  }
}

package co.bitfinder.awair.mqtt.session

import akka.actor.{Actor, ActorRef, ActorSystem, PoisonPill, Props}
import akka.testkit.{ImplicitSender, TestActorRef, TestFSMRef, TestKit, TestProbe}
import co.bitfinder.awair.mqtt.connection.MqttConnectionActor.{ConnectWithEntity, SubscribeWithEntity}
import co.bitfinder.awair.mqtt.session.SessionActor.{DirectSubscription, SessionConnected, SessionConnectedBag}
import co.bitfinder.awair.mqtt.session.SessionManagerSupervisor.GetDeviceSession
import co.bitfinder.awair.{Device, User}
import co.bitfinder.mqtt.{Connect, ConnectFlags, Header, Subscribe}
import org.scalatest.{BeforeAndAfter, FlatSpecLike, Matchers}

import scala.concurrent.duration.{FiniteDuration, _}
import scala.util.Try
/**
  * Created by jeado on 2017. 2. 7..
  */
class SessionActorSpec extends TestKit(ActorSystem("Test")) with ImplicitSender with FlatSpecLike with Matchers with BeforeAndAfter {
  implicit val timeout: FiniteDuration = 1.seconds

  val deviceType = "awair"
  val deviceId = 1
  val userId = 0

  val topic = s"org/559b45ef956608eb31b05db9/device/$deviceType/$deviceId/event/sensor/format/json"
  var sessionManagerSupervisor: ActorRef = _
  var deviceSess: TestActorRef[SessionActor] = _
  var appSess: TestActorRef[SessionActor] = _
  val probe = TestProbe()

  val defaultConnFlag = ConnectFlags(true, false, false, 0, false, true, 0)
  val deviceConn = ConnectWithEntity(Connect(Header(), defaultConnFlag, "clientId", None, None, None, None), Device(deviceType, deviceId))
  val appConn = ConnectWithEntity(Connect(Header(), defaultConnFlag, "clientId", None, None, None, None), User(userId))

  before {
    sessionManagerSupervisor = system.actorOf(Props(new Actor() {
      override def receive: Receive = {
        case _: GetDeviceSession => sender ! deviceSess
      }
    }), "sessionManagerSupervisor")
    deviceSess = TestFSMRef(new SessionActor(testActor, sessionManagerSupervisor, None), s"device-$deviceType-$deviceId")
    appSess = TestFSMRef(new SessionActor(testActor, sessionManagerSupervisor, None), s"user-$userId")
    probe watch sessionManagerSupervisor
    probe watch deviceSess
    probe watch appSess

    deviceSess ! deviceConn
    appSess ! appConn
  }

  after {
    deviceSess ! PoisonPill
    Try(probe.expectTerminated(deviceSess))
    appSess ! PoisonPill
    Try(probe.expectTerminated(appSess))
    sessionManagerSupervisor ! PoisonPill
    Try(probe.expectTerminated(sessionManagerSupervisor))
  }

  it should "be waiting for new session and change to SessionConnected with Connect" in {
    awaitAssert({
      deviceSess.underlyingActor.stateName shouldBe SessionConnected
      appSess.underlyingActor.stateName shouldBe SessionConnected
    }, 3.seconds)
  }

  it should "store direct subscriptions when a new subscription comes in" in {
    appSess ! SubscribeWithEntity(Subscribe(Header(), 2, Vector((topic, 0))), Device(deviceType, deviceId))
    awaitAssert({
      deviceSess.underlyingActor.stateData.asInstanceOf[SessionConnectedBag].subs.head shouldBe DirectSubscription(topic, appSess, 0)
    }, 3.seconds)
  }

  it should "remove subscription when the target actor is destroyed" in {
    appSess ! SubscribeWithEntity(Subscribe(Header(), 2, Vector((topic, 0))), Device(deviceType, deviceId))
    appSess ! PoisonPill
    probe.expectTerminated(appSess)

    deviceSess.underlyingActor.stateData.asInstanceOf[SessionConnectedBag].subs.length shouldBe 0
  }

  it should "ignore topics that do not conform to the topic rule or has an wildcard" in {
    def assertNumOfSubscriptions(numOfSubs: Int) = awaitAssert({
      deviceSess.underlyingActor.stateData.asInstanceOf[SessionConnectedBag].subs.length shouldBe numOfSubs
    }, 3.seconds)

    assertNumOfSubscriptions(0)
    appSess ! SubscribeWithEntity(Subscribe(Header(), 2, Vector((topic, 0))), Device(deviceType, deviceId))
    assertNumOfSubscriptions(1)

    appSess ! SubscribeWithEntity(Subscribe(Header(), 2, Vector(("sys/*", 0))), Device(deviceType, deviceId))
    assertNumOfSubscriptions(1)

    appSess ! SubscribeWithEntity(Subscribe(Header(), 2, Vector((s"/device/$deviceType/+/event/sensor/format/json", 0))), Device(deviceType, deviceId))
    assertNumOfSubscriptions(1)
  }
}

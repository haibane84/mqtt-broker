logLevel := Level.Warn
resolvers += Resolver.typesafeRepo("releases")

addSbtPlugin("com.typesafe.sbt" % "sbt-aspectj" % "0.9.4")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.4")
addSbtPlugin("com.lightbend.sbt" % "sbt-javaagent" % "0.1.2")
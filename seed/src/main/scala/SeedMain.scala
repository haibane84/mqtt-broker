import akka.actor.{ActorSystem, Props}
import co.bitfinder.mqtt.NodeConfig
import org.slf4j.LoggerFactory

/**
  * Created by jeado on 2017. 1. 5..
  */
object SeedMain extends App {
  val logger = LoggerFactory.getLogger(SeedMain.getClass)
  val nodeConfig = NodeConfig parse args

  nodeConfig foreach { c =>
    logger.info(s"Start ActorSystem ${c.clusterName} with seedNodes : ${c.seedNodes}")

    val system = ActorSystem(c.clusterName, c.config)

    system.log.info(s"ActorSystem ${system.name} started successfully")
  }
}

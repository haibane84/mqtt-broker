#!/usr/bin/env bash
export KAFKA_ADVERTISED_HOST_NAME='localhost'
docker-compose rm -f
docker-compose build
docker-compose up
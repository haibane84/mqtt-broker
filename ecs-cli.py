#!/usr/bin/python

import sys
import getopt
import subprocess
import re
import json

SERVICE = "awair-mqtt-broker"
STG_ECS_CLUSTER = "stg-awair-mqtt-broker"
PRD_ECS_CLUSTER = "prd-awair-mqtt-broker"

class Docker:

	def build_image(self):
		built_result, _ = subprocess.Popen(["sbt", "backend/docker:publishLocal"], stdout=subprocess.PIPE).communicate()
		built_image = re.search(r"Built image .+", built_result).group()[12:-4]
		return built_image

	def tag_image(self, src, dst):
		subprocess.Popen(["docker", "tag", src, dst]).communicate()
		return dst

	def push_image(self, img):
		subprocess.Popen(["docker", "push", img]).communicate()
		return img


class AwsEcs:
	
	ECR_URL = "148512310086.dkr.ecr.us-west-2.amazonaws.com"

	def __init__(self, env):
		self.env = env

	def login(self):
		aws_ecr_login_str, _ = subprocess.Popen(["aws", "ecr", "get-login", "--region", "us-west-2"], stdout=subprocess.PIPE).communicate()
		subprocess.call(aws_ecr_login_str, shell=True)

	def get_image(self, service, tag):
		return self.ECR_URL + "/" + service + ":" + tag

	def get_cluster(self):
		if self.env == "stg":
			return STG_ECS_CLUSTER
		elif self.env == "prd":
			return PRD_ECS_CLUSTER
		else:
			raise Error("Unsupported environment")

	def update_task(self, service, image):
		desc = json.loads(subprocess.Popen(["aws", "ecs", "describe-task-definition", "--task-definition", self.env + "-" + service], stdout=subprocess.PIPE).communicate()[0])["taskDefinition"]
		filter(lambda x: x["name"] == service, desc["containerDefinitions"])[0]["image"] = image
		desc.pop("status", None)
		desc.pop("requiresAttributes", None)
		desc.pop("taskDefinitionArn", None)
		desc.pop("revision", None)
		return json.loads(subprocess.Popen(["aws", "ecs", "register-task-definition", "--network-mode", "host", "--cli-input-json", json.dumps(desc)], stdout=subprocess.PIPE).communicate()[0])["taskDefinition"]

	def update_service(self, service, task):
		desc = json.loads(subprocess.Popen(["aws", "ecs", "describe-services", "--cluster", self.get_cluster(), "--services", service], stdout=subprocess.PIPE).communicate()[0])["services"][0]
		desc.pop("status", None)
		desc.pop("pendingCount", None)
		desc.pop("loadBalancers", None)
		desc.pop("roleArn", None)
		desc.pop("createdAt", None)
		desc.pop("serviceName", None)
		desc.pop("serviceArn", None)
		desc.pop("deployments", None)
		desc.pop("events", None)
		desc.pop("runningCount", None)
		desc.pop("clusterArn", None)
		desc.pop("placementConstraints", None)
		desc.pop("placementStrategy", None)
		return json.loads(subprocess.Popen(["aws", "ecs", "update-service", "--cluster", self.get_cluster(), "--service", service, "--task-definition", task, "--cli-input-json", json.dumps(desc)], stdout=subprocess.PIPE).communicate()[0])["service"]


def main(argv):
	usage = 'ecs-cli.py -e(--env) <stg|prd>'
	env = None
	try:
		opts, args = getopt.getopt(argv, "h:e:", ["env="])
	except getopt.GetoptError:
		print usage
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print usage
			sys.exit()
		elif opt in ("-e", "--env"):
			env = arg

	docker = Docker()
	ecs = AwsEcs(env)

	built_image = docker.build_image()
	print ">>> Docker image " + built_image + " was built"
	ecs.login()
	print ">>> AWS ECR login succeeded"
	tagged_image = docker.tag_image(built_image, ecs.get_image(SERVICE, built_image.split(":")[1]))
	pushed_image = docker.push_image(tagged_image)
	print ">>> AWS ECR image " + pushed_image + " pushed"
	updated_task_desc = ecs.update_task(SERVICE, pushed_image)
	print ">>> AWS ECS task " + updated_task_desc["taskDefinitionArn"] + " created"
	updated_service_desc = ecs.update_service(SERVICE, updated_task_desc["taskDefinitionArn"])
	print ">>> AWS ECS service " + updated_service_desc["serviceName"] + " updated"


if __name__ == "__main__":
	main(sys.argv[1:])

package co.bitfinder.mqtt

import java.nio.charset.StandardCharsets

import akka.io.Tcp
import akka.util.ByteString
import scodec.{Attempt, Codec}
import scodec.bits.{BitVector, ByteVector}

object Helpers {

  implicit class ByteStringHelper(val s: ByteString) extends AnyVal {
    def toBitVector: BitVector = BitVector(s)
  }

  implicit class BitVectorHelper(val a: Attempt[BitVector]) extends AnyVal {
    def toTcpWrite: Tcp.Write = Tcp.Write(ByteString(a.require.toByteBuffer))

    def toTcpReceived: Tcp.Received = Tcp.Received(ByteString(a.require.toByteBuffer))
  }

  implicit class PacketHelper(val a: Packet) extends AnyVal {
    def toTcpReceived: Tcp.Received = Helpers.toTcpReceived(a)

    def toByteString: ByteString = Helpers.toTcpReceived(a).data
  }

  def toTcpReceived(a: Packet): Tcp.Received = Tcp.Received(ByteString(Codec[Packet].encode(a).require.toByteBuffer))

  implicit class StringHelper(val s: String) extends AnyVal {

    def toBin: Array[Byte] = s.replaceAll("[^0-9A-Fa-f]", "").sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte)

    def toByteVector: ByteVector = ByteVector.encodeString(s)(StandardCharsets.UTF_8) match {
      case Right(res) => return res
      case Left(x) => throw x
    }

    def toByteString: ByteString = ByteString(s.toBin)

    def toTcpReceived: Tcp.Received = Tcp.Received(s.toByteString)

    def toTcpWrite: Tcp.Write = Tcp.Write(s.toByteString)
  }

}

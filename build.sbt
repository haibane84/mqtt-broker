import com.typesafe.sbt.packager.MappingsHelper.directory

val akkaVersion = "2.4.16"
val playVersion = "2.5.8"
val kamonVersion = "0.6.3"
val aspectjweaver = "1.8.5"
val akkaHttpVersion = "10.0.2"

organization in ThisBuild := "co.bitfinder"
version in ThisBuild := "1.0.2"
scalaVersion in ThisBuild := "2.11.7"

val commonSettings = Seq(
  resolvers ++= Seq(
    "Maven Public" at "https://repo1.maven.org/maven2/",
    "Maven Central" at "http://repo1.maven.org/maven/",
    "Eclipse Paho Repo" at "https://repo.eclipse.org/content/repositories/paho-releases/",
    "Bintray Mosquette Repo" at "http://dl.bintray.com/andsel/maven/",
    "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
  ),
  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-metrics" % akkaVersion,
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "org.scodec" %% "scodec-core" % "1.7.1",
    "org.scodec" %% "scodec-bits" % "1.0.9",
    "com.typesafe.akka" %% "akka-stream-kafka" % "0.11-M3",
    "ch.qos.logback" % "logback-classic" % "1.1.6",
    "com.github.danielwegener" % "logback-kafka-appender" % "0.1.0",
    "com.typesafe.play" %% "play" % playVersion,
    "com.typesafe.play" %% "play-ws" % playVersion,
    "com.jason-goodwin" %% "authentikat-jwt" % "0.4.1",
    "io.kamon" %% "kamon-core" % kamonVersion,
    "io.kamon" %% "kamon-datadog" % kamonVersion,
    "io.kamon" %% "kamon-log-reporter" % kamonVersion,
    "io.kamon" %% "kamon-system-metrics" % kamonVersion,
    "io.kamon" % "sigar-loader" % "1.6.6-rev002",
    "org.aspectj" % "aspectjweaver" % "1.8.9",
    "com.typesafe.play" %% "play-test" % playVersion % "test",
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test",
    "org.scalatest" %% "scalatest" % "2.2.1" % "test",
    "org.mockito" % "mockito-core" % "1.9.0" % "test",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.2.1" % "test",
    "org.specs2" %% "specs2-core" % "3.6.4" % "test"
  ),
  parallelExecution in Test := false
)


lazy val seed = (project in file("./seed")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Seq(
      "com.amazonaws" % "aws-java-sdk" % "1.11.13",
      "com.github.scopt" %% "scopt" % "3.2.0"
    ),
    dockerExposedPorts := Seq(2551)
  )

lazy val core = (project in file("./core"))
  .settings(commonSettings: _*)

lazy val awair = (project in file("./awair"))
  .settings(commonSettings: _*)
  .dependsOn(core)

lazy val backend = (project in file("./backend"))
  .enablePlugins(JavaAppPackaging)
  .settings(commonSettings: _*)
  .settings(
    dockerExposedPorts := Seq(1883, 2551),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
      "de.heikoseeberger" %% "akka-http-play-json" % "1.12.0"
    ),
    packageName := "awair-mqtt-broker"
  )
  .dependsOn(core)
  .dependsOn(seed)
  .dependsOn(awair)

lazy val admin = (project in file("./admin"))
  .enablePlugins(JavaAppPackaging)
  .settings(commonSettings: _*)
  .settings(
    mappings in Universal ++= directory("admin/ui"),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
      "de.heikoseeberger" %% "akka-http-play-json" % "1.12.0"
    ),
    dockerExposedPorts := Seq(9022)
  )
  .dependsOn(core)
  .dependsOn(backend)
  .dependsOn(seed)

lazy val root = (project in file(".")).
  aggregate(core, awair, seed, backend)
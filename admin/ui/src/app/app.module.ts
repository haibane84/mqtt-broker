import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import 'hammerjs';

import { AppComponent } from './app.component';
import { SessionsComponent } from './sessions/sessions.component';
import {SessionService} from "./sessions/shared/session.service";
import {EventBusService} from "./event-bus/shared/event-bus.service";
import { SessionComponent } from './sessions/session/session.component';
import { EventBusComponent } from './event-bus/event-bus.component';

@NgModule({
  declarations: [
    AppComponent,
    SessionsComponent,
    SessionComponent,
    EventBusComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot()
  ],
  providers: [
    SessionService,
    EventBusService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    SessionComponent
  ]
})
export class AppModule { }

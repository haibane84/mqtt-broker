import { Component, OnInit } from '@angular/core';
import {EventBusService} from "./shared/event-bus.service";
import {EventBusInfo} from "./shared/event-bus-info.medel";

@Component({
  selector: 'app-event-bus',
  templateUrl: './event-bus.component.html',
  styleUrls: ['./event-bus.component.css']
})
export class EventBusComponent implements OnInit {
  totalSubscriptions: number;
  eventBusInfo: EventBusInfo = new EventBusInfo();

  constructor(public eventBusService: EventBusService) { }

  ngOnInit() {
    this.eventBusService.getEventBus()
      .subscribe(r => this.eventBusInfo = r);
  }

}

/**
 * Created by gojaedo on 2017. 3. 2..
 */
export class EventBusInfo {
  subscriptions: Subscription[] = [];
  retains: Retain[] = [];
}

export class Subscription {
  topic: string;
  qos: number;
  session: string;
}

export class Retain {
  topic: string
}

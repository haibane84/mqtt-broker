import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {EventBusInfo} from "./event-bus-info.medel";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable()
export class EventBusService {

  constructor(private http: Http) { }

  getEventBus(): Observable<EventBusInfo> {
    return this.http.get(`${environment.apiUrl}/v1/event-bus`)
      .map(r => r.json())
      .map(r => {
        let re = new EventBusInfo();
        re.subscriptions = r.subscriptions;
        re.retains = r.retains;
        console.log(re);
        return re;
      })
  }

}

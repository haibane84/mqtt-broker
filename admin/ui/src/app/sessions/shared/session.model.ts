export class Session {
  messageId: number;
  lastPacket: string;
  cleanSession: boolean;
  connection: string;
}

import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {Session} from "./session.model";
import {SessionsPath} from "./sessions-path.model";

@Injectable()
export class SessionService {

  constructor(private http: Http) { }

  getSession(path:string): Observable<Session> {
    return this.http.get(`${environment.apiUrl}/v1/sessions/${path}`)
      .map(r => r.json())
      .map(r => {
        let re = new Session();
        re.cleanSession = r.clean_session;
        re.connection = r.connection;
        re.lastPacket = r.last_packet;
        re.messageId = r.message_id;
        return re;
      })
  }

  getSessionsPath(): Observable<SessionsPath> {
    return this.http.get(`${environment.apiUrl}/v1/sessions`)
      .map(r => r.json());
  }

}

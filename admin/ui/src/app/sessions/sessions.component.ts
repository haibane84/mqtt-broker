import { Component, OnInit } from '@angular/core';
import {SessionService} from "./shared/session.service";
import {SessionsPath} from "./shared/sessions-path.model";
import {MdDialog} from "@angular/material";
import {SessionComponent} from "./session/session.component";

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.css']
})
export class SessionsComponent implements OnInit {
  private sessionsPaths: SessionsPath[];
  private totalSessions: number = 0;

  constructor(public sessionService: SessionService, public dialog: MdDialog) { }

  ngOnInit() {
    this.sessionService.getSessionsPath()
      .subscribe(this.bindResultAndGetTotal)
  }

  openDialog(clientId: string) {
    this.sessionService.getSession(clientId)
      .subscribe(r => {
        let dialogRef = this.dialog.open(SessionComponent);
        dialogRef.componentInstance.session = r;
        dialogRef.componentInstance.clientId = clientId;
      });
  }

  private bindResultAndGetTotal = (r) => {
    this.sessionsPaths = r;
    var total = 0;
    for (let obj of r) {
      total+=obj.sessions.length;
      obj.sessions = obj.sessions.map(s => s.replace("akka://awairMqtt/user/sessionManager/", ""));
    }
    this.totalSessions = total;
  }



}

import { Component, OnInit } from '@angular/core';
import {MdDialogRef} from "@angular/material";
import {Session} from "../shared/session.model";

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {
  session: Session;
  clientId: string;
  constructor(public dialogRef: MdDialogRef<SessionComponent>) {}

  ngOnInit() {
  }

}

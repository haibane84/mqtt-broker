package co.bitfinder.mqtt.admin

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern._
import akka.routing.{ActorSelectionRoutee, FromConfig, GetRoutees, Routees}
import akka.util.Timeout
import co.bitfinder.awair.mqtt.session.SessionsManagerActor.GetSessions
import co.bitfinder.mqtt.admin.BrokerClient.{ReqSessions, SessionInfo}

import scala.concurrent.duration._
import scala.concurrent.Future

/**
  * Created by jeado on 2017. 6. 26..
  */
object BrokerClient {
  case object ReqSessions
  case class SessionInfo(path: String, sessions: Seq[String])
  def props = Props[BrokerClient]
}

class BrokerClient extends Actor with ActorLogging {
  implicit val timeout = Timeout(10 seconds)
  import context.dispatcher
  val sessionManagerRouter: ActorRef = context.actorOf(FromConfig.props(Props.empty), "sessionManagerRouter")

  override def receive: Receive = {
    case ReqSessions =>
      val sessions = for {
        routees <- (sessionManagerRouter ? GetRoutees).map { case x: Routees => x }
        sessions <- Future.sequence {
          routees.routees.map { r =>
            val manager = r.asInstanceOf[ActorSelectionRoutee]
            (manager.selection ? GetSessions()).map {
              case list: Seq[String] => SessionInfo(manager.selection.anchorPath.toString, list)
            }
          }
        }
      } yield sessions
      sessions.pipeTo(sender())
  }
}

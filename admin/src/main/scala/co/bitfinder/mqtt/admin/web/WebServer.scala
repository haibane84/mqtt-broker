package co.bitfinder.mqtt.admin.web

import akka.actor.{Actor, ActorRef, ActorSystem}
import akka.cluster.client.ClusterClient
import akka.http.scaladsl.server.Directives
import akka.pattern.ask
import akka.routing.{ActorSelectionRoutee, GetRoutees, Routees}
import akka.util.Timeout
import co.bitfinder.mqtt.admin.domain.Empty
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import org.joda.time.{DateTime, DateTimeZone}
import org.joda.time.format.{DateTimeFormat, ISODateTimeFormat}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

trait WebServer extends Directives with PlayJsonSupport {
  val logger = LoggerFactory.getLogger(this.getClass)
  implicit val timeout = Timeout(200 seconds)

  def actorSystem: ActorSystem

  def clusterClient: ActorRef

//  implicit val subscriptionWrites: Writes[SubInfo] = Writes { s =>
//    Json.obj(
//      "topic" -> s.topic,
//      "qos" -> s.qos,
//      "session" -> s.sessionUrl
//    )
//  }
//
//  implicit val retainWrites: Writes[Retain] = Writes { s =>
//    Json.obj(
//      "topic" -> s.topic
//    )
//  }


  def route =
    path("") {
      getFromFile("admin/ui/dist/index.html")
    } ~ {
      getFromDirectory("admin/ui/dist")
    }
//    pathPrefix("v1") {
//      withRequestTimeout(50 seconds) {
//        (path("sessions") & get) {
//          complete(
//            (clusterClient ? ClusterClient.SendToAll("/user/sessionManagerSupervisor", GetRoutees)).map {
//              case e: Routees =>
//                val sessionsF = e.routees.map(d => {
//                  val manager = d.asInstanceOf[ActorSelectionRoutee]
//                  (manager.selection ? GetSessions()).map {
//                    case list: Seq[String] =>
//                      logger.info("sessiogs => {}", list)
//                      Json.obj(
//                        "path" -> manager.selection.anchorPath.toString,
//                        "sessions" -> list
//                      )
//                  }
//                })
//                Future.sequence(sessionsF).map {
//                  case s: Vector[JsObject] => s
//                }
//            }
//          )
//        } ~
//          (path("sessions" / Remaining) & get) { session =>
//            complete(
//              (clusterClient ? ClusterClient.SendToAll(s"/user/sessionManager/$session", GetSessionInfo)).map {
//                case bag: SessionWaitingBag =>
//                  Json.obj(
//                    "message_id" -> bag.message_id,
//                    "clean_session" -> bag.clean_session
//                  )
//                case bag: SessionConnectedBag =>
//                  val time = new DateTime(bag.last_packet).withZone(DateTimeZone.UTC).toString
//                  Json.obj(
//                    "message_id" -> bag.message_id,
//                    "last_packet" -> time,
//                    "clean_session" -> bag.clean_session,
//                    "connection" -> bag.connection.path.toString
//                  )
//              }
//            )
//          } ~
//          path("event-bus") {
//            complete(
//              (clusterClient.?(ClusterClient.SendToAll(s"/user/event-bus-proxy", GetBusInfo))(Timeout(20 seconds))).map {
//                case a: (List[SubInfo], List[Retain]) =>
//                  Json.obj(
//                    "subscriptions" -> a._1,
//                    "retains" -> a._2
//                  )
//              }
//            )
//          }
//      }
//    }
}
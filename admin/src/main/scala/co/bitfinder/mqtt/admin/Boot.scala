package co.bitfinder.mqtt.admin

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.pattern.ask
import akka.stream.ActorMaterializer
import co.bitfinder.mqtt.NodeConfig
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import co.bitfinder.mqtt.admin.BrokerClient.{ReqSessions, SessionInfo}
import com.typesafe.config.ConfigFactory
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import play.api.libs.json._

import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Boot extends App with PlayJsonSupport {
  implicit val timeout = Timeout(10 seconds)
  val nodeConfig = NodeConfig parse args
  val jwtSecret = ConfigFactory.load().getString("jwt.secret")

  nodeConfig match {
    case Some(c) =>
      implicit val system = ActorSystem(c.clusterName, c.config)
      implicit val materializer = ActorMaterializer()
      implicit val executionContext = system.dispatcher
      implicit val sessionInfoFormat = Json.format[SessionInfo]

      val brokerClient = system.actorOf(BrokerClient.props, "brokerClient")
      val route = {
        path("") {
          getFromFile("admin/ui/dist/index.html")
        } ~ {
          getFromDirectory("admin/ui/dist")
        } ~ (path("sessions") & get) {
          complete {
            (brokerClient ? ReqSessions).map {
              case s: Vector[SessionInfo] => s
            }
          }
        }
      }
      val bindingFuture = Http().bindAndHandle(route, "0.0.0.0", 9022)

      bindingFuture onComplete {
        case Success(_) ⇒
          println(s"Server is listening on 0.0.0.0:9022")
        case Failure(e) ⇒
          println(s"Binding failed with ${e.getMessage}")
          system.terminate()
      }

    case None =>
      println(s"Configuration is wrong. Shutdown the system")
      System.exit(0)
  }
}

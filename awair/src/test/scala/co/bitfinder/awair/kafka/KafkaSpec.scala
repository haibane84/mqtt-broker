package co.bitfinder.awair.kafka

import co.bitfinder.mqtt.{Header, Publish}
import org.joda.time.{DateTime, DateTimeUtils}
import org.scalatest.FlatSpec
import KafkaProducerService._

/**
 * Created by jaykim on 5/25/16.
 */
class KafkaSpec extends FlatSpec {
  val header = Header(false, 0, false)

  "producerRecords" should "return records of proper Kafka topics for MQTT messages" in {
    DateTimeUtils.setCurrentMillisFixed(new DateTime("2016-09-02T08:13:41.325Z").getMillis)
    val pub = Publish(
      header,
      "org/559b45ef956608eb31b05db9/device/awair/1/event/sensor/format/json",
      0,
      "{\"temp\":20.0}")
    val res = producerRecords(pub)
    assert(res.size == 2)
    assert(res.map(_.topic).toSet == Set("awair-mqtt", "device.awair.event.sensor"))
    assert(res.map(_.value) == List(
      "{\"topic\":\"org/559b45ef956608eb31b05db9/device/awair/1/event/sensor/format/json\",\"createdDate\":\"2016-09-02T08:13:41.325Z\",\"payload\":{\"temp\":20}}",
      "{\"deviceType\":\"awair\",\"deviceId\":\"1\",\"eventType\":\"sensor\",\"payload\":{\"temp\":20},\"timestamp\":\"2016-09-02T08:13:41.325Z\"}"))
  }

  it should "exclude payload that is not of JSON format" in {
    val pub = new Publish(
      header,
      "org/559b45ef956608eb31b05db9/device/awair/1/event/sensor/format/json",
      0,
      "NOT JSON")
    val res = producerRecords(pub)
    assert(res.isEmpty)
  }
}

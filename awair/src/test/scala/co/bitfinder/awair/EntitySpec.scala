package co.bitfinder.awair

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by jay on 9/16/16.
  */
class EntitySpec extends FlatSpec with Matchers {
  val token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdfaWQiOiI1NTliNDVlZjk1NjYwOGViMzFiMDVkYjkiLCJkZXZpY2VfdHlwZSI6ImF3YWlyIiwiZGV2aWNlX2lkIjoiMzQ2MiIsImlhdCI6MTQ1ODUzNDU4Nn0.ircKKcb7jc6BIRWXtIDVbEYi80HYQzqtJ-djcL9cE4Y"

  "Entity" should "return the JWT token to the correspondent Entity" in {
    val secret = "bitfinder4ever!"
    Entity.fromJwt(token, secret) shouldBe Some(Device("awair", 3462))
  }

  it should "return None if the JWT token is unverified because the secret is inappropriate" in {
    val secret = "inappropriate"
    Entity.fromJwt(token, secret) shouldBe None
  }
}

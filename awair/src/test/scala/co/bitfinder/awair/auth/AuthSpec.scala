package co.bitfinder.awair.auth

import co.bitfinder.awair.{Device, User, auth}
import co.bitfinder.mqtt._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}
import play.api.libs.json.Json
import play.api.mvc.{Action, Results}
import play.api.test.WsTestClient
import play.core.server.Server
import play.api.routing.sird._

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by jay on 9/14/16.
  */
class AuthSpec extends FlatSpec with Matchers with MockFactory {

  private val awaitMax = 6.seconds

  private val device = Device("awair", 0)
  private val user = User(0)

  "authorise" should "return true for a device with Connect" in {
    assert(auth.authorise(Device("awair", 0), Connect(Header(false, 0, false), null, "")))
  }

  it should "return true for a device with Publish to its own event topic" in {
    val header = Header(false, 0, false)
    assert(auth.authorise(device, Publish(header, "org/559b45ef956608eb31b05db9/device/awair/0/event/sensor/format/json", 0, "")))
    assert(!auth.authorise(device, Publish(header, "org/559b45ef956608eb31b05db9/device/awair/0/command/display/format/json", 0, "")))
    assert(!auth.authorise(device, Publish(header, "org/559b45ef956608eb31b05db9/device/awair/1/event/sensor/format/json", 0, "")))
  }

  it should "return true for a device with Subscribe to its own event and command topics" in {
    val header = Header(false, 0, false)
    assert(auth.authorise(device, Subscribe(header, 0, Vector(("org/559b45ef956608eb31b05db9/device/awair/0/event/sensor/format/json", 0)))))
    assert(auth.authorise(device, Subscribe(header, 0, Vector(("org/559b45ef956608eb31b05db9/device/awair/0/command/sensor/format/json", 0)))))
    assert(!auth.authorise(device, Subscribe(header, 0, Vector(("org/559b45ef956608eb31b05db9/device/awair/1/event/sensor/format/json", 0)))))
    assert(!auth.authorise(device, Subscribe(header, 0, Vector(("org/559b45ef956608eb31b05db9/device/awair/0/command/sensor/format/json", 0), ("org/559b45ef956608eb31b05db9/device/awair/1/command/sensor/format/json", 0)))))
  }

  it should "return true for a user with Publish to the command topics of his/her Awair devices" in {
    val header = Header(false, 0, false)
    Server.withRouter() {
      case GET(p"/v1/admin-ownerships/awair/1") => Action {
        Results.Ok(Json.obj("user_id" -> 0))
      }
    } { implicit port =>
      WsTestClient.withClient { implicit client =>
        val authF = auth.authorise(
          user,
          Publish(
            header,
            "org/559b45ef956608eb31b05db9/device/awair/1/command/display/format/json",
            0,
            ""))
        Await.result(authF, awaitMax) shouldBe true
      }
    }
  }

  it should "return true for a user with Publish to the command topics of his/her devices that are not Awair" in {
    val header = Header(false, 0, false)
    Server.withRouter() {
      case GET(p"/v1/admin-ownerships/awair-glow/1") => Action {
        Results.Ok(Json.obj("user_id" -> 0))
      }
    } { implicit port =>
      WsTestClient.withClient { implicit client =>
        val authF = auth.authorise(
          user,
          Publish(
            header,
            "org/559b45ef956608eb31b05db9/device/awair-glow/1/command/display/format/json",
            0,
            ""))
        Await.result(authF, awaitMax) shouldBe true
      }
    }
  }

  it should "return true for a user with Subscribe to the event topics of his/her Awair devices" in {
    val header = Header(false, 0, false)
    Server.withRouter() {
      case GET(p"/v1/admin-ownerships/awair/1") => Action {
        Results.Ok(Json.obj("user_id" -> 0))
      }
      case GET(p"/v1/admin-ownerships/awair/2") => Action {
        Results.Ok(Json.obj("user_id" -> 0))
      }
    } { implicit port =>
      WsTestClient.withClient { implicit client =>
        val topics = Vector(
          ("org/559b45ef956608eb31b05db9/device/awair/1/event/score/format/json", 0),
          ("org/559b45ef956608eb31b05db9/device/awair/1/event/score/format/json", 0),
          ("org/559b45ef956608eb31b05db9/device/awair/2/event/score/format/json", 0))
        Await.result(auth.authorise(user, Subscribe(header, 0, topics)), awaitMax) shouldBe true
      }
    }
  }

  it should "return false for a user if the event topics that the user is trying to subscribe to belongs to a device that the user does not own" in {
    val header = Header(false, 0, false)
    Server.withRouter() {
      case GET(p"/v1/admin-ownerships/awair/1") => Action {
        Results.Ok(Json.obj("user_id" -> 0))
      }
      case GET(p"/v1/admin-ownerships/awair/2") => Action {
        Results.UnprocessableEntity
      }
    } { implicit port =>
      WsTestClient.withClient { implicit client =>
        val topics = Vector(
          ("org/559b45ef956608eb31b05db9/device/awair/1/event/score/format/json", 0),
          ("org/559b45ef956608eb31b05db9/device/awair/1/event/score/format/json", 0),
          ("org/559b45ef956608eb31b05db9/device/awair/2/event/score/format/json", 0))
        Await.result(auth.authorise(user, Subscribe(header, 0, topics)), awaitMax) shouldBe false
      }
    }
  }

  it should "return false for a user with Subscribe to the command topics of his/her Awair devices" in {
    val header = Header(false, 0, false)
    Server.withRouter() {
      case GET(p"/v1/admin-ownerships/awair/1") => Action {
        Results.Ok(Json.obj("user_id" -> 0))
      }
    } { implicit port =>
      WsTestClient.withClient { implicit client =>
        val topics = Vector(("org/559b45ef956608eb31b05db9/device/awair/1/command/score/format/json", 0))
        Await.result(auth.authorise(user, Subscribe(header, 0, topics)), awaitMax) shouldBe false
      }
    }
  }
}
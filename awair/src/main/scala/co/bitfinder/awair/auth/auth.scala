package co.bitfinder.awair

import co.bitfinder.mqtt.{Packet, Publish, Subscribe}
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory
import play.api.libs.ws.WSClient

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future

/**
  * Created by jay on 9/14/16.
  */
package object auth {
  private val logger = LoggerFactory.getLogger(this.getClass)

  private val ownershipServiceUrl = ConfigFactory.load().getString("ownership.service.url")

  def isAdmin(userId: Int, device: Device)(implicit ws: WSClient): Future[Boolean] = device match {
    case Device(deviceType, id) =>
      ws.url(s"$ownershipServiceUrl/v1/admin-ownerships/$deviceType/$id").get()
        .map { res =>
          res.status match {
            case 200 => (res.json \ "user_id").asOpt[Int].contains(userId)
            case 422 => false
            case code => throw new Exception(s"Unexpected status code $code")
          }
        }
  }

  def authorise(device: Device, msg: Packet): Boolean = msg match {
    // A device can publish to its event topics only
    case pub: Publish =>
      Topic.fromMqttTopic(pub.topic) match {
        case Some(DeviceEvent(d, _)) if d == device => true
        case _ => false
      }

    // A device can subscribe to its own event and command topics only
    case sub: Subscribe =>
      def isSelfTopic(topic: String): Boolean = Topic.fromMqttTopic(topic) match {
        case Some(DeviceEvent(d, _)) if d == device => true
        case Some(DeviceCommand(d, _)) if d == device => true
        case _ => false
      }
      sub.topics.map(_._1).forall(isSelfTopic)

    case _ =>
      true
  }

  def authorise(user: User, msg: Packet)(implicit ws: WSClient): Future[Boolean] = msg match {
    // A user can only publish to his/her devices' command topics
    case pub: Publish =>
      Topic.fromMqttTopic(pub.topic) match {
        case Some(DeviceCommand(d, _)) => isAdmin(user.id, d)
        case _ => Future.successful(false)
      }

    // A user can subscribe to his/her devices' event topics only
    case sub: Subscribe =>
      val devices = sub.topics.map(x => Topic.fromMqttTopic(x._1)).collect {
        case Some(DeviceEvent(d, _)) => Some(d)
        case _ => None
      }.toSet

      if (devices.contains(None))
        Future.successful(false)
      else
        Future.reduce(devices.flatten.map(d => isAdmin(user.id, d)))(_ && _)

    case _ =>
      Future.successful(true)
  }

  def authorise(entityO: Option[Entity], msg: Packet)(implicit ws: WSClient): Future[Boolean] = entityO match {
    case Some(Org) => Future.successful(true)
    case Some(device: Device) => Future.successful(authorise(device, msg))
    case Some(user: User) => authorise(user, msg)
    case None => Future.successful(false)
  }
}

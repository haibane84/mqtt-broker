package co.bitfinder.awair

import scala.util.Try

/**
  * Created by jay on 9/14/16.
  */
sealed trait Topic {
  val device: Device
}
case class DeviceEvent(device: Device, typ: String) extends Topic
case class DeviceCommand(device: Device, typ: String) extends Topic

object Topic {

  def fromMqttTopic(topic: String): Option[Topic] = Try {
    topic.split("/").toList match {
      case "org" :: "559b45ef956608eb31b05db9" :: "device" :: deviceType :: deviceId :: "event" :: eventType :: "format" :: "json" :: Nil
        if !isWildcard(deviceType) || !isWildcard(deviceId) || !isWildcard(eventType) =>
        Some(DeviceEvent(Device(deviceType, deviceId.toInt), eventType))
      case "org" :: "559b45ef956608eb31b05db9" :: "device" :: deviceType :: deviceId :: "command" :: commandType :: "format" :: "json" :: Nil
        if !isWildcard(deviceType) || !isWildcard(deviceId) || !isWildcard(commandType) =>
        Some(DeviceCommand(Device(deviceType, deviceId.toInt), commandType))
      case "device" :: deviceType :: deviceId :: "event" :: eventType :: Nil
        if !isWildcard(deviceType) || !isWildcard(deviceId) || !isWildcard(eventType) =>
        Some(DeviceEvent(Device(deviceType, deviceId.toInt), eventType))
      case "device" :: deviceType :: deviceId :: "command" :: commandType :: Nil
        if !isWildcard(deviceType) || !isWildcard(deviceId) || !isWildcard(commandType) =>
        Some(DeviceCommand(Device(deviceType, deviceId.toInt), commandType))
      case _ =>
        None
    }
  }.toOption.flatten

  def isWildcard(segment: String): Boolean = if (segment == "+" || segment == "#") true else false
}
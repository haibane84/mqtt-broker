package co.bitfinder.awair

import java.net.URLEncoder

import authentikat.jwt.JsonWebToken
import org.slf4j.LoggerFactory

import scala.util.Try

/**
  * Created by jay on 9/14/16.
  */
sealed trait Entity {
  def buildActorNameBy(clientId: String): String = {
    assert(clientId.nonEmpty, "Client ID should not empty")
    val actorName = this match {
      case Org => s"org-$clientId"
      case User(id) => s"user-$id-$clientId"
      case Device(t, id) => s"device-$t-$id"
    }
    URLEncoder.encode(actorName, "utf-8")
  }
  def toMediatorTopic = {
    this match {
      case Org => Entity.BROKER_ORG_TOPIC
      case _: Device => Entity.BROKER_DEVICE_TOPIC
      case _: User => Entity.BROKER_USER_TOPIC
    }
  }
}

case object Org extends Entity
case class User(id: Int) extends Entity
case class Device(`type`: String, id: Int) extends Entity

object Entity {
  private val logger = LoggerFactory.getLogger(Entity.getClass)
  val BROKER_ORG_TOPIC = "broker-org-topic"
  val BROKER_DEVICE_TOPIC = "broker-device-topic"
  val BROKER_USER_TOPIC = "broker-org-topic"

  def fromJwt(token: String, secret: String): Option[Entity] = {
    logger.debug(s"try to build Entity from JWT $token, $secret")
    val validTokenO = if (JsonWebToken.validate(token, secret))
      Some(token)
    else None
    validTokenO flatMap {
      case JsonWebToken(_, claimsSet, _) => claimsSet.asSimpleMap.toOption
      case _ => None
    } flatMap { m =>
      Try {
        (m.get("org_id"), m.get("device_type"), m.get("device_id").map(_.toInt), m.get("user_id").map(_.toInt)) match {
          case (Some(_), None, None, None) => Some(Org)
          case (_, Some(typ), Some(id), None) => Some(Device(typ, id))
          case (_, None, None, Some(id)) => Some(User(id))
          case _ => None
        }
      }.toOption.flatten
    }
  }
}
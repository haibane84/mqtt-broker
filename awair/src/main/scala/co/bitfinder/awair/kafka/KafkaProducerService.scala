package co.bitfinder.awair.kafka

import java.util.Properties

import akka.actor.{Actor, ActorLogging, Props}
import co.bitfinder.awair.{Device, DeviceCommand, DeviceEvent, Topic}
import co.bitfinder.mqtt.Publish
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.joda.time.{DateTime, DateTimeZone}
import org.slf4j.LoggerFactory
import play.api.libs.json._

import scala.util.Try

/**
  * Created by jeado on 2016. 6. 29..
  */
object KafkaProducerService {
  val log = LoggerFactory.getLogger(this.getClass)

  def props(props: Properties) = Props(new KafkaProducerService(new KafkaProducer[String, String](props)))

  def producerRecords(pub: Publish): Seq[ProducerRecord[String, String]] = {
    val str = new String(pub.payload.toArray)
    Try(Json.parse(str)).toOption match {
      case Some(json) =>
        val timestamp = new DateTime(DateTimeZone.UTC).toDateTimeISO.toString
        val legacyPayload = Json.obj(
          "topic" -> pub.topic,
          "createdDate" -> timestamp,
          "payload" -> json).toString()
        Topic.fromMqttTopic(pub.topic) match {
          case Some(DeviceEvent(Device(typ, id), evtTyp)) =>
            val payload = Json.obj(
              "deviceType" -> typ,
              "deviceId" -> id.toString,
              "eventType" -> evtTyp,
              "payload" -> json,
              "timestamp" -> timestamp).toString
            val records = List(new ProducerRecord[String, String](s"device.$typ.event.$evtTyp", payload))

            if (evtTyp == "sensor" && typ == "awair") {
              new ProducerRecord[String, String]("awair-mqtt", legacyPayload) :: records
            } else {
              records
            }
          case Some(DeviceCommand(Device(typ, id), cmdTyp)) =>
            val payload = Json.obj(
              "deviceType" -> typ,
              "deviceId" -> id.toString,
              "commandType" -> cmdTyp,
              "payload" -> json,
              "timestamp" -> timestamp
            ).toString
            List(new ProducerRecord[String, String](s"device.$typ.command.$cmdTyp", payload))
          case _ =>
            List()
        }
      case None =>
        log.warn(s"Detected a malformed JSON payload `$str` from the topic `${pub.topic}`")
        List()
    }
  }
}

class KafkaProducerService(producer: KafkaProducer[String, String]) extends Actor with ActorLogging {
  import KafkaProducerService._

  override def receive: Receive = {
    case pub: Publish =>
      producerRecords(pub).foreach(rec => producer.send(rec))
    case _ =>

  }

  override def postStop = {
    producer.close()
    super.postStop()
  }
}